package com.neusoft.zjop.service.util;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.jasig.cas.client.authentication.AttributePrincipal;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class CasInterceptor extends AbstractInterceptor {

	private static final long serialVersionUID = -4791341741096318497L;

	private Logger log = Logger.getLogger(CasInterceptor.class);

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {

		boolean flag= false;
		
		try {

			HttpServletRequest request = ServletActionContext.getRequest();

			/* 设置CAS返回的用户权限ID */
			String roleId = "";
			AttributePrincipal principal = (AttributePrincipal) request
					.getUserPrincipal();

			if (principal != null) {
				Map<String, Object> attributes = principal.getAttributes();
				roleId = (String) attributes.get("role_id");
				
				log.info("当前用户权限为: " + roleId);
			}

			if (null != roleId && "0".equals(roleId)) {
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		

		return flag ? invocation.invoke() : "error";
	}

}
