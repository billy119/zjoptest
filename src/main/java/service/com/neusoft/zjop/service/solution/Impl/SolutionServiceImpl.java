package com.neusoft.zjop.service.solution.Impl;

import java.util.List;

import com.neusoft.zjop.dao.Dao;
import com.neusoft.zjop.domain.Solution;
import com.neusoft.zjop.service.solution.SolutionService;

/**
 * <b>Application describing: 解决方案Service实现类</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * 
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-5-12 上午11:03:50
 */
public class SolutionServiceImpl implements SolutionService {

	private Dao dao;

	@Override
	public Solution getSolutionDetail(Solution queryCondition) throws Exception {
		return (Solution) dao.getSingleObject("Solution.getSolutionDetail",
				queryCondition);
	}

	@Override
	public Integer removeSolution(Solution toBeRemoved) throws Exception {
		return dao.deleteSingleObject("Solution.deleteSolution", toBeRemoved);
	}

	@Override
	public Integer replaceSolution(Solution toBeInsertedOrUpdated)
			throws Exception {
		return dao.updateSingleObject("Solution.replaceSolution",
				toBeInsertedOrUpdated);
	}

	@Override
	public Integer searchCount(Solution queryCodition) throws Exception {
		return (Integer) dao.getSingleObject("Solution.getSolutionCount", queryCodition);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Solution> searchSolutions(Solution queryCondition,
			int skipResults, int maxResults) throws Exception {
		return (List<Solution>) dao.getObjectsByPage("Solution.getSolutions",
				null, skipResults, maxResults);
	}

	/**
	 * @return the dao
	 */
	public Dao getDao() {
		return dao;
	}

	/**
	 * @param dao
	 *            the dao to set
	 */
	public void setDao(Dao dao) {
		this.dao = dao;
	}

}
