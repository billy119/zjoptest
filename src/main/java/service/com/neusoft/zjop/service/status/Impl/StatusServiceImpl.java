package com.neusoft.zjop.service.status.Impl;

import com.neusoft.zjop.dao.Dao;
import com.neusoft.zjop.domain.Problem_Status;
import com.neusoft.zjop.service.status.StatusService;

/**
 * <b>Application describing: 问题状态Service实现类</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * 
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-5-6 上午09:43:29
 */
public class StatusServiceImpl implements StatusService {

	private Dao dao;

	@Override
	public Integer getStatusCount(Problem_Status queryCondition)
			throws Exception {

		return (Integer) dao.getSingleObject("Status.getStatusCount", queryCondition);
	}

	/**
	 * @return the dao
	 */
	public Dao getDao() {
		return dao;
	}

	/**
	 * @param dao
	 *            the dao to set
	 */
	public void setDao(Dao dao) {
		this.dao = dao;
	}

}
