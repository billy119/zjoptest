package com.neusoft.zjop.service.solution;

import java.util.List;

import com.neusoft.zjop.domain.Solution;

/**
 * 
 * <b>Application describing: 解决方案Service</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * 
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-4-29 上午10:58:10
 */
public interface SolutionService {

	/**
	 * 根据传入的条件查询解决方案List
	 * @param queryCondition
	 * @param skipResults 当页的开始记录
	 * @param maxResults 每页条数
	 * @return 查询结果List
	 * @throws Exception
	 */
	public List<Solution> searchSolutions(Solution queryCondition,
			int skipResults, int maxResults)
			throws Exception;

	/**
	 * 根据传入的条件查询符合条件的解决方案种类数
	 * 
	 * @param queryCodition
	 * @return 符合条件的解决方案种类数
	 * @throws Exception
	 */
	public Integer searchCount(Solution queryCodition) throws Exception;

	/**
	 * 根据id查询解决方案详细信息
	 * 
	 * @param queryCondition
	 * @return 解决方案详细信息po
	 * @throws Exception
	 */
	public Solution getSolutionDetail(Solution queryCondition) throws Exception;

	/**
	 * 插入或更新解决方案实体对象
	 * 
	 * @param toBeInsertedOrUpdated
	 * @return 影响的列数(0代表操作失败，1代表插入成功，2代表更新成功)
	 * @throws Exception
	 */
	public Integer replaceSolution(Solution toBeInsertedOrUpdated)
			throws Exception;

	/**
	 * 根据传入的解决方案实体对象的ID属性删除该解决方案类型
	 * 
	 * @param toBeRemoved
	 * @return 影响的列数(0代表删除失败，1代表删除成功)
	 * @throws Exception
	 */
	public Integer removeSolution(Solution toBeRemoved) throws Exception;

}
