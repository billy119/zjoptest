package com.neusoft.zjop.service.util;

import java.util.List;

import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.WritableFont.FontName;

/**
 * @description 导出数据、设置等持久化对象 <br>
 *              p.s. 样式等有需要则赋值，没有需要则会自动采用默认样式
 * @author <a href="mailto:li-zr@neusoft.com"> Zongrui. Li </a>
 * @version 下午04:42:02
 */
public class ExportPo {
	private String fileName; // 文件名

	private String sheetName; // sheet名

	private String[] headNames; // 表头字符串数组

	private List<?> dataTable; // 数据

	private FontName fontName; // 字体名

	private Integer fontSize; // 字体大小

	private Boolean boldStyle; // 字体粗细 true 为粗 false 为细

	private Boolean italic; // 字体是否为斜体字

	private UnderlineStyle underlineStyle; // 字体下划线样式

	private Colour fontColor; // 字体颜色

	private Border borderStyle; // 表格边框样式

	private BorderLineStyle borderLineStyle; // 表格边框线样式

	private Colour cellColor; // 表格边框颜色

	private Colour cellBackgroundColor; // 表格填充色

	private Alignment alignment; // 表格文字调整

	// 表头项：

	private FontName headFontName; // 表头字体名

	private Integer headFontSize; // 表头字体大小

	private Boolean headBoldStyle; // 表头字体粗细 true 为粗 false 为细

	private Boolean headItalic; // 表头字体是否为斜体字

	private UnderlineStyle headUnderlineStyle; // 表头字体下划线样式

	private Colour headFontColor; // 表头字体颜色

	private Border headBorderStyle; // 表头边框样式

	private BorderLineStyle headBorderLineStyle; // 表头边框线样式

	private Colour headCellColor; // 表头边框颜色

	private Colour headCellBackgroundColor; // 表头填充色

	private Alignment headAlignment; // 表头文字调整

	/* getter & setter */
	public String getFileName() {
		return fileName;
	}

	public ExportPo setFileName(String fileName) {
		this.fileName = fileName;
		return this;
	}

	public String getSheetName() {
		return sheetName;
	}

	public ExportPo setSheetName(String sheetName) {
		this.sheetName = sheetName;
		return this;
	}

	public String[] getHeadNames() {
		return headNames;
	}

	public ExportPo setHeadNames(String[] headNames) {
		this.headNames = headNames;
		return this;
	}

	public List<?> getDataTable() {
		return dataTable;
	}

	public ExportPo setDataTable(List<?> dataTable) {
		this.dataTable = dataTable;
		return this;
	}

	public FontName getFontName() {
		return fontName;
	}

	public ExportPo setFontName(FontName fontName) {
		this.fontName = fontName;
		return this;
	}

	public Integer getFontSize() {
		return fontSize;
	}

	public ExportPo setFontSize(Integer fontSize) {
		this.fontSize = fontSize;
		return this;
	}

	public Boolean getBoldStyle() {
		return boldStyle;
	}

	public ExportPo setBoldStyle(Boolean boldStyle) {
		this.boldStyle = boldStyle;
		return this;
	}

	public Boolean getItalic() {
		return italic;
	}

	public ExportPo setItalic(Boolean italic) {
		this.italic = italic;
		return this;
	}

	public UnderlineStyle getUnderlineStyle() {
		return underlineStyle;
	}

	public ExportPo setUnderlineStyle(UnderlineStyle underlineStyle) {
		this.underlineStyle = underlineStyle;
		return this;
	}

	public Colour getFontColor() {
		return fontColor;
	}

	public ExportPo setFontColor(Colour fontColor) {
		this.fontColor = fontColor;
		return this;
	}

	public Border getBorderStyle() {
		return borderStyle;
	}

	public ExportPo setBorderStyle(Border borderStyle) {
		this.borderStyle = borderStyle;
		return this;
	}

	public BorderLineStyle getBorderLineStyle() {
		return borderLineStyle;
	}

	public ExportPo setBorderLineStyle(BorderLineStyle borderLineStyle) {
		this.borderLineStyle = borderLineStyle;
		return this;
	}

	public Colour getCellColor() {
		return cellColor;
	}

	public ExportPo setCellColor(Colour cellColor) {
		this.cellColor = cellColor;
		return this;
	}

	public Colour getCellBackgroundColor() {
		return cellBackgroundColor;
	}

	public ExportPo setCellBackgroundColor(Colour cellBackgroundColor) {
		this.cellBackgroundColor = cellBackgroundColor;
		return this;
	}

	public Alignment getAlignment() {
		return alignment;
	}

	public ExportPo setAlignment(Alignment alignment) {
		this.alignment = alignment;
		return this;
	}

	public FontName getHeadFontName() {
		return headFontName;
	}

	public ExportPo setHeadFontName(FontName headFontName) {
		this.headFontName = headFontName;
		return this;
	}

	public Integer getHeadFontSize() {
		return headFontSize;
	}

	public ExportPo setHeadFontSize(Integer headFontSize) {
		this.headFontSize = headFontSize;
		return this;
	}

	public Boolean getHeadBoldStyle() {
		return headBoldStyle;
	}

	public ExportPo setHeadBoldStyle(Boolean headBoldStyle) {
		this.headBoldStyle = headBoldStyle;
		return this;
	}

	public Boolean getHeadItalic() {
		return headItalic;
	}

	public ExportPo setHeadItalic(Boolean headItalic) {
		this.headItalic = headItalic;
		return this;
	}

	public UnderlineStyle getHeadUnderlineStyle() {
		return headUnderlineStyle;
	}

	public ExportPo setHeadUnderlineStyle(UnderlineStyle headUnderlineStyle) {
		this.headUnderlineStyle = headUnderlineStyle;
		return this;
	}

	public Colour getHeadFontColor() {
		return headFontColor;
	}

	public ExportPo setHeadFontColor(Colour headFontColor) {
		this.headFontColor = headFontColor;
		return this;
	}

	public Border getHeadBorderStyle() {
		return headBorderStyle;
	}

	public ExportPo setHeadBorderStyle(Border headBorderStyle) {
		this.headBorderStyle = headBorderStyle;
		return this;
	}

	public BorderLineStyle getHeadBorderLineStyle() {
		return headBorderLineStyle;
	}

	public ExportPo setHeadBorderLineStyle(BorderLineStyle headBorderLineStyle) {
		this.headBorderLineStyle = headBorderLineStyle;
		return this;
	}

	public Colour getHeadCellColor() {
		return headCellColor;
	}

	public ExportPo setHeadCellColor(Colour headCellColor) {
		this.headCellColor = headCellColor;
		return this;
	}

	public Colour getHeadCellBackgroundColor() {
		return headCellBackgroundColor;
	}

	public ExportPo setHeadCellBackgroundColor(Colour headCellBackgroundColor) {
		this.headCellBackgroundColor = headCellBackgroundColor;
		return this;
	}

	public Alignment getHeadAlignment() {
		return headAlignment;
	}

	public ExportPo setHeadAlignment(Alignment headAlignment) {
		this.headAlignment = headAlignment;
		return this;
	}

}
