package com.neusoft.zjop.service.util;

import com.neusoft.zjop.domain.Problem;

/**
 * <b>Application describing: 文本域操作类</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * 
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-5-12 下午05:05:36
 */
public class TextUtil {

	private static final int stringStandardlength = 15;// 自定义标准字符串长

	/**
	 * 字符串省略
	 * 
	 * @param str
	 * @return
	 */
	public static String stringEllipsis(String str) {
		if (null != str && str.length() > stringStandardlength)
			str = str.substring(0, stringStandardlength).concat("...");
		return null == str ? "" : str;
	}

	/**
	 * 问题专用trim方法
	 * 
	 * @param problem
	 * @return
	 */
	public static Problem trimProblem(Problem problem) {

		if (null != problem) {
			if (null != problem.getFault_description()) {
				problem.setFault_description(problem.getFault_description()
						.trim());
			}

			if (null != problem.getReport_person()) {
				problem.setReport_person(problem.getReport_person().trim());
			}

			if (null != problem.getResult()) {
				problem.setResult(problem.getResult().trim());
			}

			if (null != problem.getSchool()) {
				problem.setSchool(problem.getSchool().trim());
			}
		}

		return problem;
	}

	/**
	 * json字符串处理，以便于页面上隐藏标签显示，从而使得js文件中可以去到json
	 * 
	 * @param json
	 * @return
	 */
	public static String jsonTranslate(String json) {

		if (null != json) {
			/* 单引号 */
			json = json.replace('\"', '\'');
			/* 标签 */
			json = json.replace("<", "&lt;");
			json = json.replace(">", "&gt;");
			json = json.replace(" ", "&nbsp;");
			json = json.replace("\n", "<br>");
			json = json.replace("&", "&amp;");
		}

		return json;
	}

}
