package com.neusoft.zjop.service.fault;

import java.util.List;

import com.neusoft.zjop.domain.Fault;

/**
 * 
 * <b>Application describing: 故障Service</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * 
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-4-29 上午10:53:21
 */
public interface FaultService {

	/**
	 * 根据传入的条件查询故障List
	 * 
	 * @param queryCondition
	 * @param skipResults
	 *            当页的开始记录
	 * @param maxResults
	 *            每页条数
	 * @return 查询结果List
	 * @throws Exception
	 */
	public List<Fault> searchFaults(Fault queryCondition, int skipResults,
			int maxResults) throws Exception;

	/**
	 * 根据传入的条件查询符合条件的故障种类数
	 * 
	 * @param queryCodition
	 * @return 符合条件的故障种类数
	 * @throws Exception
	 */
	public Integer searchCount(Fault queryCodition) throws Exception;

	/**
	 * 根据id查询故障详细信息
	 * 
	 * @param queryCondition
	 * @return 故障详细信息po
	 * @throws Exception
	 */
	public Fault getFaultDetail(Fault queryCondition) throws Exception;

	/**
	 * 插入或更新故障实体对象
	 * 
	 * @param toBeInsertedOrUpdated
	 * @return 影响的列数(0代表操作失败，1代表插入成功，2代表更新成功)
	 * @throws Exception
	 */
	public Integer replaceFault(Fault toBeInsertedOrUpdated) throws Exception;

	/**
	 * 根据传入的故障实体对象的ID属性删除该故障类型<br>
	 * 删除时须确认是否有问题选取了该故障类型：<br>
	 * 若有，则警告，并不予以删除；<br>
	 * 若没有，则删除
	 * 
	 * @param toBeRemoved
	 * @return 影响的列数(0代表删除失败，1代表删除成功)
	 * @throws Exception
	 */
	public Integer removeFault(Fault toBeRemoved) throws Exception;

}
