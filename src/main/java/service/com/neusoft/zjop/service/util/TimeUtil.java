package com.neusoft.zjop.service.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * <b>Application describing: 时间戳转换工具</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * 
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-5-6 下午05:01:56
 */
public class TimeUtil {

	/**
	 * 返回传入时间当天的最后一秒
	 * 
	 * @return 当天时间的23时59分59秒
	 */
	public static Timestamp nextOclock(Timestamp t) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Calendar cal = Calendar.getInstance();

		cal.setTimeInMillis(t.getTime());

		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MINUTE, 59);

		String time = df.format(cal.getTime());

		return Timestamp.valueOf(time);
	}

	/**
	 * 当天时间的0时0分0秒
	 * 
	 * @return 当天时间的0时0分0秒
	 */
	public static Timestamp zeroOclock() {

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = df.format(cal.getTime());

		return Timestamp.valueOf(time);
	}

	/**
	 * 去掉时间
	 * 
	 * @param t
	 * @return
	 */
	public static String removeMillionSecond(Timestamp t) {

		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(t.getTime());

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(cal.getTime());
	}

	/**
	 * 返回一个月前的当前时间时间戳
	 * 
	 * @return
	 */
	public static Timestamp oneMonthAgoTimeStamp() {

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Calendar cal = Calendar.getInstance();

		cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - 1);

		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);

		String time = df.format(cal.getTime());

		return Timestamp.valueOf(time);
	}

	/**
	 * 返回一个星期前的当前时间时间戳
	 * 
	 * @return
	 */
	public static Timestamp oneWeekAgoTimeStamp() {

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Calendar cal = Calendar.getInstance();

		cal.set(Calendar.WEEK_OF_YEAR, cal.get(Calendar.WEEK_OF_YEAR) - 1);

		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);

		String time = df.format(cal.getTime());

		return Timestamp.valueOf(time);

	}

	/**
	 * 测试
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(TimeUtil.nextOclock(TimeUtil.zeroOclock()));
		System.out.println(TimeUtil.zeroOclock());
		System.out.println(TimeUtil.removeMillionSecond(TimeUtil
				.nextOclock(TimeUtil.zeroOclock())));
		System.out.println(TimeUtil.oneMonthAgoTimeStamp());
		System.out.println(TimeUtil.oneWeekAgoTimeStamp());
	}
}
