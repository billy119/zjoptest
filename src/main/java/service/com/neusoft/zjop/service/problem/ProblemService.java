package com.neusoft.zjop.service.problem;

import java.util.List;

import com.neusoft.zjop.domain.Fault;
import com.neusoft.zjop.domain.Problem;
import com.neusoft.zjop.domain.Problem_Status;
import com.neusoft.zjop.domain.School;
import com.neusoft.zjop.domain.Solution;

/**
 * <b>Application describing: 问题Service </b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * 
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-4-29 上午10:36:12
 */
public interface ProblemService {

	/**
	 * 根据传入的条件查询问题List
	 * 
	 * @param queryCondition
	 * @param skipResults
	 *            当页的开始记录
	 * @param maxResults
	 *            每页条数
	 * @return 查询结果List
	 * @throws Exception
	 */
	public List<Problem> searchProblems(Problem queryCondition,
			int skipResults, int maxResults) throws Exception;

	/**
	 * 根据传入的条件查询单个问题
	 * 
	 * @param queryCondition
	 * @return 查询出来的问题对象
	 * @throws Exception
	 */
	public Problem getSingleProblem(Problem queryCondition) throws Exception;

	/**
	 * 根据传入的条件查询符合条件的问题个数
	 * 
	 * @param queryCondition
	 * @return 符合条件的问题个数
	 * @throws Exception
	 */
	public Integer searchCount(Problem queryCondition) throws Exception;

	/**
	 * 插入或更新传入的问题实体对象
	 * 
	 * @param toBeInsertedOrUpdated
	 * @return 影响的列数(0代表操作失败，1代表插入成功，2代表更新成功)
	 * @throws Exception
	 */
	public Integer replaceProblem(Problem toBeInsertedOrUpdated)
			throws Exception;

	/**
	 * 根据传入的问题实体对象的GUID属性删除该问题
	 * 
	 * @param toBeRemoved
	 * @return 影响的列数(0代表删除失败，1代表删除成功)
	 * @throws Exception
	 */
	public Integer removeProblem(Problem toBeRemoved) throws Exception;

	/**
	 * 批量更改问题状态
	 * 
	 * @param guids
	 *            要修改的问题的GUID组成的字符串
	 * @param toUpdated
	 *            要改为的状态
	 * @throws Exception
	 */
	public void updateProblemsStatus(String guids, Problem toUpdated)
			throws Exception;

	/**
	 * 查询在页面上需要展示的故障类型
	 * 
	 * @return 所有故障List
	 * @throws Exception
	 */
	public List<Fault> getFaults() throws Exception;

	/**
	 * 查询在页面上需要展示的解决方案
	 * 
	 * @return 所有解决方案List
	 * @throws Exception
	 */
	public List<Solution> getSolutions() throws Exception;

	/**
	 * 查询在页面上需要展示的问题状态
	 * 
	 * @return 所有问题状态List
	 * @throws Exception
	 */
	public List<Problem_Status> getProblemStatus() throws Exception;

	/**
	 * 动态获取页面上的学校下拉框
	 * 
	 * @param queryCondition
	 * @return 符合条件的学校List
	 * @throws Exception
	 */
	public List<School> getSchools(School queryCondition) throws Exception;

	/**
	 * 用于学校ID和名称转换
	 * 
	 * @param queryCondition
	 * @return 学校对象(单个)
	 * @throws Exception
	 */
	public School getSchoolByNameOrId(School queryCondition) throws Exception;

	/**
	 * 用于学校名称校验
	 * 
	 * @param queryCondition
	 * @return 学校对象(可能为多个)
	 * @throws Exception
	 */
	public List<School> validateSchool(School queryCondition) throws Exception;

	/**
	 * 导出符合条件的问题列表Excel文件
	 * 
	 * @param queryCondition
	 * @return 查询出来的问题集合
	 * @throws Exception
	 */
	public List<Problem> exportProblems(Problem queryCondition)
			throws Exception;

}
