package com.neusoft.zjop.service.problem.Impl;

import java.util.List;

import com.neusoft.zjop.dao.Dao;
import com.neusoft.zjop.domain.Fault;
import com.neusoft.zjop.domain.Problem;
import com.neusoft.zjop.domain.Problem_Status;
import com.neusoft.zjop.domain.School;
import com.neusoft.zjop.domain.Solution;
import com.neusoft.zjop.service.problem.ProblemService;

/**
 * <b>Application describing: 问题Service实现</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * 
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-5-4 上午10:55:14
 */
public class ProblemServiceImpl implements ProblemService {

	private Dao dao;

	@SuppressWarnings("unchecked")
	@Override
	public List<Problem> exportProblems(Problem queryCondition) throws Exception {
		return (List<Problem>) dao.getObjects("Problem.getProblems",
				queryCondition);
	}

	@Override
	public void updateProblemsStatus(String guids, Problem toUpdated)
			throws Exception {

		String[] guid = guids.split(",");

		for (int i = 0; i < guid.length; i++) {
			Problem p = new Problem();
			p.setGuid(guid[i]);
			p.setProblem_status(toUpdated.getProblem_status());
			dao.updateSingleObject("Problem.updateProblemStatus", p);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Fault> getFaults() throws Exception {
		return (List<Fault>) dao.getObjects("Problem.getFaults", null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Problem_Status> getProblemStatus() throws Exception {
		return (List<Problem_Status>) dao.getObjects(
				"Problem.getProblemStatus", null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<School> getSchools(School queryCondition) throws Exception {
		return (List<School>) dao.getObjects("Problem.getSchools",
				queryCondition);
	}

	@Override
	public School getSchoolByNameOrId(School queryCondition) throws Exception {
		return (School) dao.getSingleObject("Problem.getSchoolByNameOrId",
				queryCondition);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<School> validateSchool(School queryCondition) throws Exception {
		return (List<School>) dao.getObjects("Problem.getSchoolByNameOrId",
				queryCondition);
	}

	@Override
	public Problem getSingleProblem(Problem queryCondition) throws Exception {
		return (Problem) dao.getSingleObject("Problem.getSingleProblem",
				queryCondition);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Solution> getSolutions() throws Exception {
		return (List<Solution>) dao.getObjects("Problem.getSolutions", null);
	}

	@Override
	public Integer removeProblem(Problem toBeRemoved) throws Exception {
		return dao.deleteSingleObject("Problem.deleteProblem", toBeRemoved);
	}

	@Override
	public Integer replaceProblem(Problem toBeInsertedOrUpdated)
			throws Exception {
		return dao.updateSingleObject("Problem.replaceProblem",
				toBeInsertedOrUpdated);
	}

	@Override
	public Integer searchCount(Problem queryCondition) throws Exception {
		return (Integer) dao.getSingleObject("Problem.getProblemCount",
				queryCondition);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Problem> searchProblems(Problem queryCondition,
			int skipResults, int maxResults) throws Exception {
		return (List<Problem>) dao.getObjectsByPage("Problem.getProblems",
				queryCondition, skipResults, maxResults);
	}

	public Dao getDao() {
		return dao;
	}

	public void setDao(Dao dao) {
		this.dao = dao;
	}

}
