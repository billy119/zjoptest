package com.neusoft.zjop.service.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.JxlWriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import sun.misc.BASE64Encoder;

/**
 * @description 导出Excel文件的公共方法
 * @author <a href="mailto:li-zr@neusoft.com"> Zongrui. Li </a>
 * @version 下午04:20:44
 */
public class Export2ExcelUtil {

	private static final long serialVersionUID = 281457L;

	private static final Logger log = Logger.getLogger("Export2ExcelUtil");

	private static int cellWidth = 17; // 单元格宽度

	private static int cellHeight = 300; // 单元格高度

	private static int headCellHeidht = 400; // 表头单元格高度

	/**
	 * 导出的公共方法
	 * 
	 * @param <T>
	 * @param exportPo
	 * @return
	 */
	public static <T> void exportExcel(ExportPo exportPo) throws Exception {
		initCellStyle(exportPo); // 初始化表格样式需要注入的持久化对象

		String fileName = exportPo.getFileName();

		String sheetName = exportPo.getSheetName();

		List<?> list = exportPo.getDataTable();

		String titleNames[] = exportPo.getHeadNames();

		HttpServletRequest request = null;

		request = ServletActionContext.getRequest();

		String agent = request.getHeader("user-agent"); // 处理浏览器间的兼容问题

		try {
			if (agent.contains("MSIE")) // ie
			{
				fileName = URLEncoder.encode(fileName, "utf-8");
			} else if (agent.contains("Firefox")) // 火狐
			{
				fileName = "=?UTF-8?B?"
						+ new String(new BASE64Encoder().encode(fileName
								.getBytes("UTF-8"))) + "?=";
			} else if (agent.contains("Safari")) // Safari
			{
				byte[] bytes = (fileName).getBytes("UTF-8"); // name.getBytes("UTF-8")处理safari的乱码问题
				fileName = new String(bytes, "ISO-8859-1"); // 各浏览器基本都支持ISO编码
			} else {
				fileName = URLEncoder.encode(fileName, "utf-8");
			}
		} catch (UnsupportedEncodingException e) {
			log.error("导出文件名转码异常");
			throw new Exception("导出文件名转码异常", e);
		}
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setHeader("Content-disposition", "attachment; filename="
				+ fileName + ".xls");
		response.setContentType("application/msexcel");

		OutputStream os = null;
		WritableWorkbook wwb = null;
		WritableSheet ws = null;
		try {
			os = response.getOutputStream();
			wwb = Workbook.createWorkbook(os);
			ws = wwb.createSheet(sheetName, 0);
			ws.getSettings().setDefaultColumnWidth(cellWidth); // 设置单元格宽度

			// 创建表头字体样式
			WritableFont wfc = new WritableFont(exportPo.getHeadFontName(),
					exportPo.getHeadFontSize(),
					exportPo.getHeadBoldStyle() ? WritableFont.BOLD
							: WritableFont.NO_BOLD, exportPo.getHeadItalic(),
					exportPo.getHeadUnderlineStyle(), exportPo
							.getHeadFontColor());
			// 创建内容字体样式1
			WritableFont wfc1 = new WritableFont(exportPo.getFontName(),
					exportPo.getFontSize(),
					exportPo.getBoldStyle() ? WritableFont.BOLD
							: WritableFont.NO_BOLD, exportPo.getItalic(),
					exportPo.getUnderlineStyle(), exportPo.getFontColor());
			// 创建内容字体样式2
			WritableFont wfc2 = new WritableFont(exportPo.getFontName(),
					exportPo.getFontSize(),
					exportPo.getBoldStyle() ? WritableFont.BOLD
							: WritableFont.NO_BOLD, exportPo.getItalic(),
					exportPo.getUnderlineStyle(), exportPo.getFontColor());

			WritableCellFormat wcfFC = new WritableCellFormat(wfc);
			WritableCellFormat wcfFC1 = new WritableCellFormat(wfc1);
			WritableCellFormat wcfFC2 = new WritableCellFormat(wfc2);

			// 设置表头单元格样式
			wcfFC.setBorder(exportPo.getHeadBorderStyle(), exportPo
					.getBorderLineStyle(), exportPo.getHeadCellColor()); // 边框
			wcfFC.setBackground(exportPo.getHeadCellBackgroundColor()); // 背景色
			wcfFC.setAlignment(exportPo.getHeadAlignment()); // 字体调整方式

			// 设置内容单元格1样式
			wcfFC1.setBorder(exportPo.getBorderStyle(), exportPo
					.getBorderLineStyle(), exportPo.getCellColor()); // 边框
			wcfFC1.setBackground(exportPo.getCellBackgroundColor()); // 背景色
			wcfFC1.setAlignment(exportPo.getAlignment()); // 字体调整方式

			// 设置内容单元格2样式
			wcfFC2.setBorder(exportPo.getBorderStyle(), exportPo
					.getBorderLineStyle(), exportPo.getCellColor()); // 边框
			wcfFC2.setBackground(exportPo.getCellBackgroundColor()); // 背景色
			wcfFC2.setAlignment(exportPo.getAlignment()); // 字体调整方式

			if (null == list || 0 == list.size()) // 如果没有数据进行导出，则写入错误信息
			{
				ws.getSettings().setDefaultColumnWidth(30); // 设置单元格宽度
				
				Label rowLabelNull = new Label(0, 0, "没有查询到相关数据", wcfFC);

				ws.addCell(rowLabelNull);
			} else {
				Object object = (list.get(0));

				Field[] fields = object.getClass().getDeclaredFields();// 一个类里说有类型的变量

				int colNum = 0;

				for (int i = 0; i < fields.length; i++) // 遍历要导出的实体类中的属性(竖列)
				{

					/*
					 * 判断方法中是否有指定注解类型的注解
					 */
					boolean hasAnnotation = fields[i]
							.isAnnotationPresent(ClassAnnotation.class);

					if (hasAnnotation) {
						/*
						 * 根据注解类型返回方法的指定类型注解
						 */
						ClassAnnotation annotation = fields[i]
								.getAnnotation(ClassAnnotation.class);

						boolean isNeedExport = annotation.isNeedExport();

						if (isNeedExport) {
							ws.setRowView(0, headCellHeidht); // 第一行Cell(表头)

							Label rowHeadLabel = new Label(colNum, 0,
									titleNames[i], wcfFC);

							ws.addCell(rowHeadLabel);

							Label rowLabel = null;

							for (int j = 0; j < list.size(); j++) // 第一行下面的Cell(查询出来的数据)
							{
								ws.setRowView(j + 1, cellHeight);

								Object obj = list.get(j);
								Method[] methods = obj.getClass()
										.getDeclaredMethods();
								for (Method method : methods) {
									String metherName = "get"
											+ fields[i].getName();
									if (metherName.toUpperCase().equals(
											method.getName().toUpperCase())) {
										Class<?> t = method.getReturnType();

										Object temp = t.getName();

										try {
											if (temp.equals("java.util.Date")) // 按属性类型对号入座
											{

												java.util.Date value = (java.util.Date) method
														.invoke(obj); // 调用getter方法获取属性值

												SimpleDateFormat sdf = new SimpleDateFormat(
														"yyyy-MM-dd");

												if (value != null) {
													// 列，行
													rowLabel = new Label(
															colNum, j + 1,
															sdf.format(value),
															wcfFC1);

													ws.addCell(rowLabel);
												} else {
													rowLabel = new Label(
															colNum, j + 1, "",
															wcfFC1);

													ws.addCell(rowLabel);
												}
											} else if (temp
													.equals("java.sql.Date")) {
												java.sql.Date value = (java.sql.Date) method
														.invoke(obj); // 调用getter方法获取属性值
												if (value != null) {
													// 列，行
													rowLabel = new Label(
															colNum, j + 1,
															value.toString(),
															wcfFC1);

													ws.addCell(rowLabel);
												} else {
													rowLabel = new Label(
															colNum, j + 1, "",
															wcfFC1);

													ws.addCell(rowLabel);
												}
											} else if (temp
													.equals("java.math.BigDecimal")) {
												java.math.BigDecimal value = (java.math.BigDecimal) method
														.invoke(obj); // 调用getter方法获取属性值
												if (value != null) {
													// 列，行
													rowLabel = new Label(
															colNum, j + 1,
															value.toString(),
															wcfFC1);

													ws.addCell(rowLabel);

													// 
												} else {
													rowLabel = new Label(
															colNum, j + 1, "",
															wcfFC1);

													ws.addCell(rowLabel);
												}
											} else if (temp
													.equals("java.lang.Integer")
													|| temp.equals("int")) {
												java.lang.Integer value = (java.lang.Integer) method
														.invoke(obj); // 调用getter方法获取属性值
												if (value != null) {
													// 列，行
													rowLabel = new Label(
															colNum, j + 1,
															value.toString(),
															wcfFC1);

													ws.addCell(rowLabel);

													// 
												} else {
													rowLabel = new Label(
															colNum, j + 1, "",
															wcfFC1);

													ws.addCell(rowLabel);
												}
											} else if (temp.equals("double")) {
												double value = (java.lang.Double) method
														.invoke(obj); // 调用getter方法获取属性值
												if (value != 0) {
													// 列，行
													rowLabel = new Label(
															colNum,
															j + 1,
															String
																	.valueOf(value),
															wcfFC1);

													ws.addCell(rowLabel);

													// 
												} else {
													rowLabel = new Label(
															colNum, j + 1, "",
															wcfFC1);

													ws.addCell(rowLabel);
												}
											} else {

												String value = (String) method
														.invoke(obj); // 调用getter方法获取属性值
												if (value != null) {

													// 列，行
													rowLabel = new Label(
															colNum, j + 1,
															value, wcfFC1);

													ws.addCell(rowLabel);
												} else {
													rowLabel = new Label(
															colNum, j + 1, "",
															wcfFC1);

													ws.addCell(rowLabel);
												}
											}
										} catch (JxlWriteException e) {
											log.error("导出时创建单元格异常", e);
											throw new Exception("导出时创建单元格异常", e);
										} catch (IllegalAccessException e) {
											log.error("导出时方法反射访问安全权限异常", e);
											throw new Exception(
													"导出时方法反射访问安全权限异常", e);
										} catch (IllegalArgumentException e) {
											log.error("导出时方法反射传入参数非法异常", e);
											throw new Exception(
													"导出时方法反射传入参数非法异常", e);
										} catch (InvocationTargetException e) {
											log.error("导出时反射构造方法异常", e);
											throw new Exception("导出时反射构造方法异常",
													e);
										}

									}
								}
							}
						}
						// this.doBeforeCreateExl(ws);

						colNum++;
					}
				}
			}
		} catch (IOException e) {
			log.error("导出IO流异常", e);
			throw new Exception("导出IO流异常", e);
		} catch (RowsExceededException e) {
			log.error("导出行扩展异常", e);
			throw new Exception("导出行扩展异常", e);
		} catch (WriteException e) {
			log.error("导出写出流异常", e);
			throw new Exception("导出写出流异常", e);
		} finally {
			if (null != wwb) {
				try {
					wwb.write();
					wwb.close();
					if (null != os) {
						os.close();
					}
				} catch (IOException e) {
					if (!"Connection reset by peer: socket write error"
							.equals(e.getCause().getMessage())) {
						log.error("关闭流时IO流异常", e);
						throw new Exception("关闭流时IO流异常", e);
					}
				} catch (WriteException e) {
					log.error("关闭流时写出流异常", e);
					throw new Exception("关闭流时写出流异常", e);
				}
			}
		}
	}

	/**
	 * 将未设置excel表格样式的字段赋值
	 * 
	 * @param po
	 */
	private static void initCellStyle(ExportPo po) {
		po
				.setFontName(
						po.getFontName() == null ? WritableFont.TIMES : po
								.getFontName())
				.setFontSize(po.getFontSize() == null ? 10 : po.getFontSize())
				.setBoldStyle(
						po.getBoldStyle() == null ? false : po.getBoldStyle())
				.setItalic(po.getItalic() == null ? false : po.getItalic())
				.setUnderlineStyle(
						po.getUnderlineStyle() == null ? UnderlineStyle.NO_UNDERLINE
								: po.getUnderlineStyle())
				.setFontColor(
						po.getFontColor() == null ? jxl.format.Colour.AUTOMATIC
								: po.getFontColor())
				.setBorderStyle(
						po.getBorderStyle() == null ? Border.ALL : po
								.getBorderStyle())
				.setBorderLineStyle(
						po.getBorderLineStyle() == null ? BorderLineStyle.THIN
								: po.getBorderLineStyle())
				.setCellColor(
						po.getCellColor() == null ? jxl.format.Colour.BLACK
								: po.getCellColor())
				.setCellBackgroundColor(
						po.getCellBackgroundColor() == null ? jxl.format.Colour.WHITE
								: po.getCellBackgroundColor())
				.setAlignment(
						po.getAlignment() == null ? jxl.format.Alignment.RIGHT
								: po.getAlignment())
				.setHeadFontName(
						po.getHeadFontName() == null ? WritableFont
								.createFont("黑体 _GB2312") : po
								.getHeadFontName())
				.setHeadFontSize(
						po.getHeadFontSize() == null ? 12 : po
								.getHeadFontSize())
				.setHeadBoldStyle(
						po.getHeadBoldStyle() == null ? true : po
								.getHeadBoldStyle())
				.setHeadItalic(
						po.getHeadItalic() == null ? false : po.getHeadItalic())
				.setHeadUnderlineStyle(
						po.getHeadUnderlineStyle() == null ? UnderlineStyle.NO_UNDERLINE
								: po.getHeadUnderlineStyle())
				.setHeadFontColor(
						po.getHeadFontColor() == null ? jxl.format.Colour.BLACK
								: po.getHeadFontColor())
				.setHeadBorderStyle(
						po.getHeadBorderStyle() == null ? Border.ALL : po
								.getHeadBorderStyle())
				.setHeadBorderLineStyle(
						po.getHeadBorderLineStyle() == null ? BorderLineStyle.THIN
								: po.getHeadBorderLineStyle())
				.setHeadCellColor(
						po.getHeadCellColor() == null ? jxl.format.Colour.BLACK
								: po.getHeadCellColor())
				.setHeadCellBackgroundColor(
						po.getHeadCellBackgroundColor() == null ? jxl.format.Colour.LIGHT_TURQUOISE
								: po.getHeadCellBackgroundColor())
				.setHeadAlignment(
						po.getHeadAlignment() == null ? jxl.format.Alignment.CENTRE
								: po.getHeadAlignment());
	}

}
