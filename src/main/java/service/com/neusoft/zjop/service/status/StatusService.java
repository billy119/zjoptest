package com.neusoft.zjop.service.status;

import com.neusoft.zjop.domain.Problem_Status;

/**
 * <b>Application describing: 问题状态Service</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * 
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-5-6 上午09:37:33
 */
public interface StatusService {

	/**
	 * 查询某个状态下的所有问题数量
	 * 
	 * @param queryCondition
	 * @return 问题数量
	 * @throws Exception
	 */
	public Integer getStatusCount(Problem_Status queryCondition)
			throws Exception;
}
