package com.neusoft.zjop.service.statistics;

import java.util.List;
import java.util.Map;

import com.neusoft.zjop.domain.Problem;

/**
 * <b>Application describing: 数据统计Service</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * 
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-5-13 上午10:15:23
 */
public interface StatisticsService {

	/**
	 * 根据时间段查询每一天的问题个数
	 * 
	 * @param queryCondition
	 * @return 日期为key,问题个数为value的HashMap
	 * @throws Exception
	 */
	public List<Map<String, String>> getData(Problem queryCondition)
			throws Exception;

}
