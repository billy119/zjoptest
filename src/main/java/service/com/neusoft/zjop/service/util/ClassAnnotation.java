package com.neusoft.zjop.service.util;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @description 导出指定列所需要的注解
 * @author <a href="mailto:li-zr@neusoft.com"> Zongrui. Li </a>
 * @version 下午01:47:46
 */

@Documented
@Target(value = { ElementType.FIELD, ElementType.METHOD, ElementType.TYPE })
@Retention(value = RetentionPolicy.RUNTIME)
public @interface ClassAnnotation {
	/**
	 * 是否需要导出
	 * 
	 * @return
	 */
	boolean isNeedExport() default false;
}
