package com.neusoft.zjop.service.fault.Impl;

import java.util.List;

import com.neusoft.zjop.dao.Dao;
import com.neusoft.zjop.domain.Fault;
import com.neusoft.zjop.service.fault.FaultService;

/**
 * <b>Application describing: 故障Service实现类</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * 
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-5-12 上午11:04:19
 */
public class FaultServiceImpl implements FaultService {

	private Dao dao;

	@Override
	public Fault getFaultDetail(Fault queryCondition) throws Exception {
		return (Fault) dao.getSingleObject("Fault.getFaultDetail",
				queryCondition);
	}

	@Override
	public Integer removeFault(Fault toBeRemoved) throws Exception {
		return dao.deleteSingleObject("Fault.deleteFault", toBeRemoved);
	}

	@Override
	public Integer replaceFault(Fault toBeInsertedOrUpdated) throws Exception {
		return dao.updateSingleObject("Fault.replaceFault",
				toBeInsertedOrUpdated);
	}

	@Override
	public Integer searchCount(Fault queryCodition) throws Exception {
		return (Integer) dao.getSingleObject("Fault.getFaultCount",
				queryCodition);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Fault> searchFaults(Fault queryCondition, int skipResults,
			int maxResults) throws Exception {
		return (List<Fault>) dao.getObjectsByPage("Fault.getFaults", null,
				skipResults, maxResults);
	}

	/**
	 * @return the dao
	 */
	public Dao getDao() {
		return dao;
	}

	/**
	 * @param dao
	 *            the dao to set
	 */
	public void setDao(Dao dao) {
		this.dao = dao;
	}

}
