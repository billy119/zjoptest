package com.neusoft.zjop.service.statistics.Impl;

import java.util.List;
import java.util.Map;

import com.neusoft.zjop.dao.Dao;
import com.neusoft.zjop.domain.Problem;
import com.neusoft.zjop.service.statistics.StatisticsService;

public class StatisticsServiceImpl implements StatisticsService {

	private Dao dao;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, String>> getData(Problem queryCondition) throws Exception {
		
		List<Map<String, String>> resultMap = (List<Map<String, String>>) dao.getObjects("Statistics.getData", queryCondition);
		
		return resultMap;
	}

	/**
	 * @return the dao
	 */
	public Dao getDao() {
		return dao;
	}

	/**
	 * @param dao the dao to set
	 */
	public void setDao(Dao dao) {
		this.dao = dao;
	}

}
