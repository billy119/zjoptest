package com.neusoft.zjop.action.problem;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.jasig.cas.client.authentication.AttributePrincipal;

import com.google.gson.Gson;
import com.neusoft.zjop.action.base.PageAction;
import com.neusoft.zjop.action.page.Page;
import com.neusoft.zjop.domain.Fault;
import com.neusoft.zjop.domain.Problem;
import com.neusoft.zjop.domain.Problem_Status;
import com.neusoft.zjop.domain.School;
import com.neusoft.zjop.domain.Solution;
import com.neusoft.zjop.service.problem.ProblemService;
import com.neusoft.zjop.service.util.Export2ExcelUtil;
import com.neusoft.zjop.service.util.ExportPo;
import com.neusoft.zjop.service.util.TextUtil;
import com.neusoft.zjop.service.util.TimeUtil;

/**
 * <b>Application describing: 问题管理相关Action</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * 
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-5-5 上午11:10:11
 */
public class ProblemAction extends PageAction {
	private static final long serialVersionUID = -3740339889547721702L;

	private Logger log = Logger
			.getLogger("com.neusoft.zjop.action.problem.ProblemAction");

	private ProblemService problemService;

	private List<Problem> problemList;// 进入问题浏览页面时用

	private List<Fault> faultList;

	private List<Solution> solutionList;

	private List<Problem_Status> statusList;

	private Problem problem; // 进入问题详细页面时或作为查询条件时用

	private School school; // 用于Ajax关联学校上下文

	private String guids; // 问题GUID字符串

	private Boolean returnNew;// 是否返回至新增问题页面

	private String statusJson;// 状态列表json

	private String bubbleMsg;// 气泡显示的提示信息： 如 “保存成功” 等 在跳转时需要显示的信息

	/**
	 * 根据条件查询问题List
	 * 
	 * @return 跳回页面
	 */
	public String getProblems() {

		if (null == problem) {
			problem = new Problem();
		}

		if (null == problemList) {
			problemList = new ArrayList<Problem>();
		}

		problem = TextUtil.trimProblem(problem); // trim 查询条件

		try {
			if (null == problem.getStart_time()) { /* 如果查询条件中没有起始和终止时间，则默认赋值当天前一周的时间段 */
				problem.setStart_time(TimeUtil.oneWeekAgoTimeStamp());
			}

			if (null == problem.getEnd_time()) {
				problem.setEnd_time(TimeUtil.zeroOclock());
			}

			problem.setEnd_time(TimeUtil.nextOclock(problem.getEnd_time())); /* 将页面上传来的结束时间变为该天的最后一秒 */

			int count = problemService.searchCount(problem); /* 分页需要，查询问题总数 */

			Page page = getPage(count);

			List<Problem> pList = problemService.searchProblems(problem, page
					.getStartOfPage(), page.getPageSize());

			log.info("问题List查询成功");

			faultList = problemService.getFaults(); /* 加载故障类型、解决方案、问题状态List */

			solutionList = problemService.getSolutions();

			statusList = problemService.getProblemStatus();

			log.info("故障类型、解决方案、问题状态List加载成功");

			problem
					.setStart_time_to_show(TimeUtil // 对时间戳类型的数据进行处理：去掉毫秒数显示
							.removeMillionSecond(null == problem
									.getStart_time() ? TimeUtil.zeroOclock()
									: problem.getStart_time()));

			problem
					.setEnd_time_to_show(TimeUtil // 对时间戳类型的数据进行处理：去掉毫秒数显示
							.removeMillionSecond(null == problem.getEnd_time() ? TimeUtil
									.zeroOclock()
									: problem.getEnd_time()));

			for (Iterator<?> i = pList.iterator(); i.hasNext();) {/* 页面上显示转换 */
				Problem temp = (Problem) i.next();
				temp = changeToShow(temp);

				problemList.add(temp);
			}

			log.info("问题List分页查询成功");

		} catch (Exception e) {
			log.error("问题List查询异常", e);
			return "error";
		}

		return "success";
	}

	/**
	 * 进入问题编辑页面时，根据条件查询单个问题
	 * 
	 * @return 跳回页面
	 */
	public String getSingleProblem() {

		try {
			faultList = problemService.getFaults(); /* 加载故障类型、解决方案、问题状态List */

			solutionList = problemService.getSolutions();

			statusList = problemService.getProblemStatus();

			Gson gson = new Gson();

			statusJson = gson.toJson(statusList);

			statusJson = TextUtil.jsonTranslate(statusJson);
			
			log.info("故障类型、解决方案、问题状态List加载成功");
		} catch (Exception e) {
			log.error("查询List异常", e);
			return "error";
		}

		/* 标识字段returnNew 判定是否为新建问题 ， 新建问题则跳转至新建问题页面 */
		if (returnNew || null == problem || null == problem.getGuid()
				|| "".equals(problem.getGuid())) {

			problem = new Problem();

			problem.setGuid(UUID.randomUUID().toString()); // 初始化guid

			problem.setProblem_status(1); // 初始化问题状态 ：已申报

			log.info("跳入新建问题页面");

			return "input";
		}

		try {
			problem = problemService.getSingleProblem(problem);

			problem = changeToShow(problem);

			log.info("guid为 " + problem.getGuid() + " 的单个问题详细信息查询成功");

		} catch (Exception e) {
			log.error("查询单个问题异常", e);
			return "error";
		}
		log.info("跳入查看单个问题页面");

		return "success";
	}

	/**
	 * 插入或更新单个问题(1个入口)
	 * 
	 * @return 跳回页面
	 */
	public String replaceSingleProblem() {

		School s = new School();// 学校转码

		s.setDept_name(problem.getSchool().trim());

		try {
			problem.setSchool(problemService.getSchoolByNameOrId(s)
					.getDept_id());
		} catch (Exception e1) {
			log.error("学校转码异常", e1);
			return "error";
		}

		if (null == problem.getReport_time()) {// 没有上报时间 说明是新建问题
			problem.setReport_time(TimeUtil.zeroOclock());
		}

		problem = TextUtil.trimProblem(problem); // trim Problem

		if (null == problem.getReport_person() // 如果上报人为空 ， 则填写系统的用户名
				|| "".equals(problem.getReport_person())) {

			HttpServletRequest request = ServletActionContext.getRequest();

			AttributePrincipal principal = (AttributePrincipal) request
					.getUserPrincipal();

			if (null != principal) {
				problem.setReport_person(principal.getName());
			}
		}
		
		try {
			problemService.replaceProblem(problem);
			log.info("guid为 " + problem.getGuid() + " 的问题添加或更新成功");
		} catch (Exception e) {
			log.error("添加或更新问题异常", e);
			return "error";
		}
		if (returnNew) { // 如果是新建，则进入新建页面("继续添加问题")
			problem = null;
		}

		log.info("重定向至getSingleProblem的Action");

		bubbleMsg = "ok";

		return "success";
	}

	/**
	 * Ajax插入或更新单个问题(2个入口：添加问题时点击保存进入)
	 * 
	 * @return 带标志位的流
	 */
	public void replaceSingleProblemByAjax() {

		HttpServletResponse resp = ServletActionContext.getResponse();
		PrintWriter out = null;
		String msg = "";

		boolean flag = true;	// 流程flag：false --> 之前的流程未通过，不继续走流程 ； true --> 之前的流程正常通过，继续走当前流程
		
		if (null == problem.getReport_time()) {// 没有上报时间 说明是新建问题
			problem.setReport_time(TimeUtil.zeroOclock());
			School s = new School(); // 学校转码
			s.setDept_name(problem.getSchool().trim());
			
			try {
				problem.setSchool(problemService.getSchoolByNameOrId(s)
						.getDept_id());
			} catch (Exception e) {
				log.error("学校查询异常", e);
				msg = "保存失败" + msg;
				flag = false;
			}
			
			problem = TextUtil.trimProblem(problem); // trim Problem  如果上报人填写为" "等无效字符串，则先进行一次trim，再填充上报人
			
			if (flag) {
				if (null == problem.getReport_person() // 如果上报人为空 ， 则填写系统的用户名
						|| "".equals(problem.getReport_person())) {

					HttpServletRequest request = ServletActionContext.getRequest();

					AttributePrincipal principal = (AttributePrincipal) request
							.getUserPrincipal();

					if (null != principal) {
						problem.setReport_person(principal.getName());
					}
				}
				
				msg += problem.getReport_person();	// 传值到页面
				
			}
		}

		problem = TextUtil.trimProblem(problem); // trim Problem

		if (flag) {
			try {
				problemService.replaceProblem(problem);
				msg = "保存成功," + msg;
				log.info("guid为 " + problem.getGuid() + " 的问题通过Ajax添加或更新成功");
			} catch (Exception e) {
				log.error("添加或更新问题异常", e);
				msg = "保存失败";
				flag = false;
			}
		}
		

		try {
			out = resp.getWriter();
			out.write(msg);
			out.flush();
			out.close();
		} catch (IOException e) {
			log.error("输出流处理异常", e);
		} finally {
			if (null != out) {
				out.close();
			}
		}
	}

	/**
	 * 删除问题(1个入口)
	 * 
	 * @return 带有标志位的输出流
	 */
	public void removeProblemByAjax() {

		HttpServletResponse resp = ServletActionContext.getResponse();
		PrintWriter out = null;
		String msg = "";

		try {
			problemService.removeProblem(problem);
			msg = "删除成功";
			log.info("guid为 " + problem.getGuid() + " 的问题删除成功");
		} catch (Exception e) {
			log.error("删除问题异常", e);
			msg = "删除失败";
		}

		try {
			out = resp.getWriter();
			out.write(msg);
			out.flush();
			out.close();
		} catch (IOException e) {
			log.error("输出流处理出错", e);
		} finally {
			if (null != out) {
				out.close();
			}
		}

	}

	/**
	 * 批量更改问题状态
	 * 
	 * @return 带有标志位的输出流
	 */
	public void updateProblemsStatusByAjax() {

		HttpServletResponse resp = ServletActionContext.getResponse();
		PrintWriter out = null;
		String msg = "";

		try {
			problemService.updateProblemsStatus(guids, problem);
			msg = "状态更新成功";
			log.info("批量更改问题状态成功");
		} catch (Exception e) {
			log.error("批量更新问题状态异常", e);
			msg = "状态更新失败";
		}

		try {
			out = resp.getWriter();
			out.write(msg);
			out.flush();
			out.close();
		} catch (IOException e) {
			log.error("输出流处理出错", e);
		} finally {
			if (null != out) {
				out.close();
			}
		}
	}

	/**
	 * 动态获取学校下拉框里的值
	 * 
	 * @return 带有数据的输出流
	 */
	public void getSchoolsByAjax() {

		if (null == school || null == school.getDept_name() // 若输入框中没有值，则显示所有学校
				|| "".equals(school.getDept_name())) {
			school = new School();
			school.setDept_name("");
		}

		HttpServletResponse resp = ServletActionContext.getResponse();
		PrintWriter out = null;
		List<School> schoolList = null;

		school.setDept_name(school.getDept_name().trim()); // trim

		try {
			schoolList = problemService.getSchools(school);
		} catch (Exception e) {
			log.error("学校查询异常", e);
		}

		try {
			Gson gson = new Gson();
			String str = gson.toJson(schoolList);
			out = resp.getWriter();
			out.write(str);
			out.flush();
			out.close();
		} catch (IOException e) {
			log.error("输出流处理出错", e);
		} finally {
			if (null != out) {
				out.close();
			}
		}
	}

	/**
	 * 校验学校是否存在(在页面上提交表单时触发)
	 */
	public void validateSchool() {
		HttpServletResponse resp = ServletActionContext.getResponse();
		PrintWriter out = null;
		List<School> re = null;

		school.setDept_name(school.getDept_name());

		try {
			re = problemService.validateSchool(school);
		} catch (Exception e) {
			log.error("校验学校异常", e);
		}

		try {
			Gson gson = new Gson();
			String str = gson.toJson(re != null && !re.isEmpty());
			out = resp.getWriter();
			str = TextUtil.jsonTranslate(str);
			out.write(str);
			out.flush();
			out.close();
		} catch (IOException e) {
			log.error("输出流处理出错", e);
		} finally {
			if (null != out) {
				out.close();
			}
		}
	}

	/**
	 * 按条件导出问题列表
	 * 
	 * @return 带有excel表格的输出流
	 */
	@SuppressWarnings("unchecked")
	public void exportProblemsByAjax() {
		if (null == problem) {
			problem = new Problem();
		}

		if (null == problemList) {
			problemList = new ArrayList<Problem>();
		}

		problem.setEnd_time(TimeUtil.nextOclock(problem.getEnd_time()));// 结束时间改为当天的最后一秒
		
		problem = TextUtil.trimProblem(problem); // trim 查询条件

		try {
			List<Problem> pList = (List<Problem>) problemService /* 查询要导出的问题 */
			.exportProblems(problem);

			log.info("查询要导出的问题List成功");

			faultList = problemService.getFaults();/* 加载故障类型、解决方案、问题状态List */

			solutionList = problemService.getSolutions();

			statusList = problemService.getProblemStatus();

			log.info("故障类型、解决方案、问题状态List加载成功");

			problem.setStart_time_to_show(TimeUtil.removeMillionSecond(problem
					.getStart_time()));

			problem.setEnd_time_to_show(TimeUtil.removeMillionSecond(problem
					.getEnd_time()));

			for (Iterator<?> i = pList.iterator(); i.hasNext();) {
				Problem temp = (Problem) i.next();
				temp = changeToShow(temp);

				problemList.add(temp);
			}

		} catch (Exception e) {
			log.error("问题List查询异常", e);
		}

		try {

			String[] titles = { "上报时间", "上报人", "学校", "故障类型", "故障描述", "处理方案", /* 导出的excel文件中的表头 */
			"处理结果", "问题状态" };

			ExportPo po = new ExportPo();
			po.setFileName("问题数据导出_" /* 文件名 */
					+ new SimpleDateFormat("yyyyMMdd").format(new Date()));
			po.setSheetName("问题数据"); /* sheet名 */
			po.setDataTable(problemList);
			po.setHeadNames(titles);
			Export2ExcelUtil.exportExcel(po);
			log.info("导出excel成功");
		} catch (Exception e) {
			log.error("导出excel异常", e);
		}

	}

	/**
	 * 页面上显示等字段转换
	 * 
	 * @param temp
	 * @throws Exception
	 */
	private Problem changeToShow(Problem temp) throws Exception {

		temp.setReport_time_to_show(TimeUtil.removeMillionSecond(temp // 上报时间
				.getReport_time()));
		School s = new School(); // 学校
		s.setDept_id(temp.getSchool());
		temp.setSchool_to_show(problemService.getSchoolByNameOrId(s)
				.getDept_name());

		for (Iterator<?> fi = faultList.iterator(); fi.hasNext();) { // 故障类型
			Fault fault = (Fault) fi.next();
			if (fault.getFault_id().equals(temp.getFault_type())) {
				temp.setFault_type_to_show(fault.getFault_name());
				break;
			}
		}

		if (null != temp.getSolution()) {
			for (Iterator<?> si = solutionList.iterator(); si.hasNext();) { // 解决方案
				Solution solution = (Solution) si.next();
				if (solution.getSolution_id().equals(temp.getSolution())) {
					temp.setSolution_to_show(solution.getSolution_name());
					break;
				}
			}
		}

		for (Iterator<?> sti = statusList.iterator(); sti.hasNext();) { // 问题状态
			Problem_Status status = (Problem_Status) sti.next();
			if (status.getStatus_id().equals(temp.getProblem_status())) {
				temp.setProblem_status_to_show(status.getStatus_name());
				break;
			}
		}

		temp.setFault_description_to_show(TextUtil.stringEllipsis(temp // 故障描述省略
				.getFault_description()));

		temp.setResult_to_show(TextUtil.stringEllipsis(temp.getResult()));// 结果省略

		return temp;
	}

	/**
	 * @return the problemService
	 */
	public ProblemService getProblemService() {
		return problemService;
	}

	/**
	 * @param problemService
	 *            the problemService to set
	 */
	public void setProblemService(ProblemService problemService) {
		this.problemService = problemService;
	}

	/**
	 * @return the faultList
	 */
	public List<Fault> getFaultList() {
		return faultList;
	}

	/**
	 * @param faultList
	 *            the faultList to set
	 */
	public void setFaultList(List<Fault> faultList) {
		this.faultList = faultList;
	}

	/**
	 * @return the solutionList
	 */
	public List<Solution> getSolutionList() {
		return solutionList;
	}

	/**
	 * @param solutionList
	 *            the solutionList to set
	 */
	public void setSolutionList(List<Solution> solutionList) {
		this.solutionList = solutionList;
	}

	/**
	 * @return the problem
	 */
	public Problem getProblem() {
		return problem;
	}

	/**
	 * @param problem
	 *            the problem to set
	 */
	public void setProblem(Problem problem) {
		this.problem = problem;
	}

	/**
	 * @return the school
	 */
	public School getSchool() {
		return school;
	}

	/**
	 * @param school
	 *            the school to set
	 */
	public void setSchool(School school) {
		this.school = school;
	}

	/**
	 * @return the guids
	 */
	public String getGuids() {
		return guids;
	}

	/**
	 * @param guids
	 *            the guids to set
	 */
	public void setGuids(String guids) {
		this.guids = guids;
	}

	/**
	 * @return the statusList
	 */
	public List<Problem_Status> getStatusList() {
		return statusList;
	}

	/**
	 * @param statusList
	 *            the statusList to set
	 */
	public void setStatusList(List<Problem_Status> statusList) {
		this.statusList = statusList;
	}

	/**
	 * @return the problemList
	 */
	public List<Problem> getProblemList() {
		return problemList;
	}

	/**
	 * @param problemList
	 *            the problemList to set
	 */
	public void setProblemList(List<Problem> problemList) {
		this.problemList = problemList;
	}

	/**
	 * @return the returnNew
	 */
	public Boolean getReturnNew() {
		return returnNew;
	}

	/**
	 * @param returnNew
	 *            the returnNew to set
	 */
	public void setReturnNew(Boolean returnNew) {
		this.returnNew = returnNew;
	}

	/**
	 * @return the statusJson
	 */
	public String getStatusJson() {
		return statusJson;
	}

	/**
	 * @param statusJson
	 *            the statusJson to set
	 */
	public void setStatusJson(String statusJson) {
		this.statusJson = statusJson;
	}

	/**
	 * @return the bubbleMsg
	 */
	public String getBubbleMsg() {
		return bubbleMsg;
	}

	/**
	 * @param bubbleMsg
	 *            the bubbleMsg to set
	 */
	public void setBubbleMsg(String bubbleMsg) {
		this.bubbleMsg = bubbleMsg;
	}

}
