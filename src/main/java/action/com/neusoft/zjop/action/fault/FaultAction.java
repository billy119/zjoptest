package com.neusoft.zjop.action.fault;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.neusoft.zjop.action.base.PageAction;
import com.neusoft.zjop.action.page.Page;
import com.neusoft.zjop.domain.Fault;
import com.neusoft.zjop.domain.Problem;
import com.neusoft.zjop.service.fault.FaultService;
import com.neusoft.zjop.service.problem.ProblemService;
import com.neusoft.zjop.service.util.TextUtil;
import com.neusoft.zjop.service.util.TimeUtil;

/**
 * <b>Application describing: 故障数据配置Action</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * 
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-5-12 上午11:45:44
 */
public class FaultAction extends PageAction {

	private static final long serialVersionUID = 153755946804342054L;

	private Logger log = Logger
			.getLogger("com.neusoft.zjop.action.fault.FaultAction");

	private FaultService faultService;

	private ProblemService problemService;

	private List<Fault> faultList;

	private Fault fault;

	private Boolean isInsert;

	private String bubbleMsg;/* 气泡信息： 条专用 */

	/**
	 * 根据条件查询故障类型
	 * 
	 * @return 跳回页面
	 */
	public String getFaults() {

		if (null == faultList) {
			faultList = new ArrayList<Fault>();
		}

		try {
			Integer count = faultService.searchCount(null);

			Page page = getPage(count);

			faultList = faultService.searchFaults(null, page.getStartOfPage(),
					page.getPageSize());

			for (Iterator<Fault> i = faultList.iterator(); i.hasNext();) {
				Fault faultTemp = (Fault) i.next();
				changeToShow(faultTemp);
			}

			log.info("故障类型查询成功");

		} catch (Exception e) {
			log.error("查询故障类型异常", e);
			return "error";
		}

		return "success";
	}

	/**
	 * 进入问题编辑页面时，根据条件查询单个故障
	 * 
	 * @return 跳回页面
	 */
	public String getSingleFault() {

		if (null == fault || null == fault.getFault_id()
				|| "".equals(fault.getFault_id())) {

			fault = new Fault();

			try {
				fault.setFault_id(faultService.searchCount(null) + 1);// 初始化fault_id
			} catch (Exception e) {
				log.error("查询故障总条数异常", e);
				return "error";
			}

			isInsert = true;

			return "success";
		}

		try {
			fault = faultService.getFaultDetail(fault);

			changeToShow(fault);

		} catch (Exception e) {
			log.error("查询单个故障异常", e);
			return "error";
		}
		isInsert = false;

		return "success";
	}

	/**
	 * 插入或更新故障类型
	 * 
	 * @return 跳回页面
	 */
	public String replaceFault() {

		if (null != fault) { /* trim */
			if (null != fault.getFault_name()) {
				fault.setFault_name(fault.getFault_name().trim());
			}
			if (null != fault.getFault_description()) {
				fault.setFault_description(fault.getFault_description().trim());
			}
		}

		try {
			faultService.replaceFault(fault);
		} catch (Exception e) {
			log.error("更新故障数据异常", e);
			return "error";
		}

		isInsert = false;
		
		bubbleMsg = "ok";

		return "success";
	}

	/**
	 * 删除故障类型
	 * 
	 * @return 带有标志位的输出流
	 */
	public void removeFaultByAjax() {

		HttpServletResponse resp = ServletActionContext.getResponse();
		PrintWriter out = null;
		String msg = "";

		try {
			Problem problem = new Problem();

			problem.setFault_type(fault.getFault_id());

			Integer count = problemService.searchCount(problem);

			if (count.equals(new Integer(0))) {
				faultService.removeFault(fault);
				msg = "删除成功";
			} else {
				fault = faultService.getFaultDetail(fault);
				msg = "仍有故障为" + fault.getFault_name() + "的问题，无法删除该故障类型";
			}
		} catch (Exception e) {
			log.error("删除数据异常", e);
			msg = "删除数据异常";
		}

		try {
			out = resp.getWriter();
			out.write(msg);
			out.flush();
			out.close();
		} catch (IOException e) {
			log.error("输出流处理异常", e);
		} finally {
			if (null != out) {
				out.close();
			}
		}
	}

	/**
	 * 页面上显示等字段转换
	 * 
	 * @param temp
	 * @return
	 */
	private void changeToShow(Fault temp) {
		temp.setFault_update_time_to_show(TimeUtil.removeMillionSecond(temp
				.getFault_update_time()));
		temp.setFault_description_to_show(TextUtil.stringEllipsis(temp
				.getFault_description()));
	}

	/**
	 * @return the faultService
	 */
	public FaultService getFaultService() {
		return faultService;
	}

	/**
	 * @param faultService
	 *            the faultService to set
	 */
	public void setFaultService(FaultService faultService) {
		this.faultService = faultService;
	}

	/**
	 * @return the faultList
	 */
	public List<Fault> getFaultList() {
		return faultList;
	}

	/**
	 * @param faultList
	 *            the faultList to set
	 */
	public void setFaultList(List<Fault> faultList) {
		this.faultList = faultList;
	}

	/**
	 * @return the fault
	 */
	public Fault getFault() {
		return fault;
	}

	/**
	 * @param fault
	 *            the fault to set
	 */
	public void setFault(Fault fault) {
		this.fault = fault;
	}

	/**
	 * @return the problemService
	 */
	public ProblemService getProblemService() {
		return problemService;
	}

	/**
	 * @param problemService
	 *            the problemService to set
	 */
	public void setProblemService(ProblemService problemService) {
		this.problemService = problemService;
	}

	/**
	 * @return the isInsert
	 */
	public Boolean getIsInsert() {
		return isInsert;
	}

	/**
	 * @param isInsert
	 *            the isInsert to set
	 */
	public void setIsInsert(Boolean isInsert) {
		this.isInsert = isInsert;
	}

	/**
	 * @return the bubbleMsg
	 */
	public String getBubbleMsg() {
		return bubbleMsg;
	}

	/**
	 * @param bubbleMsg
	 *            the bubbleMsg to set
	 */
	public void setBubbleMsg(String bubbleMsg) {
		this.bubbleMsg = bubbleMsg;
	}

}
