package com.neusoft.zjop.action.remote;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.neusoft.zjop.domain.Fault;
import com.neusoft.zjop.domain.Problem;
import com.neusoft.zjop.service.problem.ProblemService;

/**
 * <b>Application describing: 远端需要调用的Action</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * 
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-5-14 下午02:33:32
 */
public class RemoteAction {

	private static final long serialVersionUID = 3602123052195894741L;

	private Logger log = Logger.getLogger(RemoteAction.class);

	private ProblemService problemService;

	/**
	 * 远端获取问题个数(已申报)
	 */
	public void getProblemLeft() {
		HttpServletResponse resp = ServletActionContext.getResponse();
		PrintWriter out = null;

		List<Fault> faultList = new ArrayList<Fault>();

		Integer count = null;

		try {
			faultList = problemService.getFaults();/* 获取故障类型List */
			Problem problem = new Problem();

			problem.setFault_type(faultList.get(0).getFault_id());/* 设置为第一个元素：即已申报 */

			count = problemService.searchCount(problem);/* 取得已申报的问题个数 */

			log.info("远端获取未处理问题个数成功");

		} catch (Exception e) {
			log.error("远端获取未解决问题个数查询异常", e);
		}

		try {
			out = resp.getWriter();
			out.write(count);
			out.flush();
			out.close();
		} catch (IOException e) {
			log.error("输出流处理异常", e);
		} finally {
			if (null != out) {
				out.close();
			}
		}

	}

	/**
	 * @return the problemService
	 */
	public ProblemService getProblemService() {
		return problemService;
	}

	/**
	 * @param problemService
	 *            the problemService to set
	 */
	public void setProblemService(ProblemService problemService) {
		this.problemService = problemService;
	}

}
