package com.neusoft.zjop.action.solution;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.neusoft.zjop.action.base.PageAction;
import com.neusoft.zjop.action.page.Page;
import com.neusoft.zjop.domain.Solution;
import com.neusoft.zjop.domain.Problem;
import com.neusoft.zjop.service.solution.SolutionService;
import com.neusoft.zjop.service.problem.ProblemService;
import com.neusoft.zjop.service.util.TextUtil;
import com.neusoft.zjop.service.util.TimeUtil;

/**
 * <b>Application describing: 解决方案数据配置Action</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * 
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-5-12 上午11:45:44
 */
public class SolutionAction extends PageAction {

	private static final long serialVersionUID = 153755946804342054L;

	private Logger log = Logger
			.getLogger("com.neusoft.zjop.action.solution.SolutionAction");

	private SolutionService solutionService;

	private ProblemService problemService;

	private List<Solution> solutionList;

	private Solution solution;

	private Boolean isInsert;

	private String bubbleMsg; /* 气泡信息 ： 跳转页面时用 */

	/**
	 * 根据条件查询解决方案类型
	 * 
	 * @return 跳回页面
	 */
	public String getSolutions() {

		if (null == solutionList) {
			solutionList = new ArrayList<Solution>();
		}

		try {
			Integer count = solutionService.searchCount(null);

			Page page = getPage(count);

			solutionList = solutionService.searchSolutions(null, page
					.getStartOfPage(), page.getPageSize());

			for (Iterator<Solution> i = solutionList.iterator(); i.hasNext();) {
				Solution solutionTemp = (Solution) i.next();
				changeToShow(solutionTemp);
			}

		} catch (Exception e) {
			log.error("查询解决方案异常", e);
			return "error";
		}

		return "success";
	}

	/**
	 * 进入问题编辑页面时，根据条件查询单个解决方案
	 * 
	 * @return 跳回页面
	 */
	public String getSingleSolution() {

		if (null == solution || null == solution.getSolution_id()
				|| "".equals(solution.getSolution_id())) {

			solution = new Solution();

			try {
				solution.setSolution_id(solutionService.searchCount(null) + 1);// 初始化solution_id
			} catch (Exception e) {
				log.error("查询解决方案总条数异常", e);
				return "error";
			}

			isInsert = true;

			return "success";
		}

		try {
			solution = solutionService.getSolutionDetail(solution);

			changeToShow(solution);

		} catch (Exception e) {
			log.error("查询单个解决方案异常", e);
			return "error";
		}
		isInsert = false;

		return "success";
	}

	/**
	 * 插入或更新解决方案类型
	 * 
	 * @return 跳回页面
	 */
	public String replaceSolution() {

		if (null != solution) { /* trim */
			if (null != solution.getSolution_name()) {
				solution.setSolution_name(solution.getSolution_name().trim());
			}

			if (null != solution.getSolution_description()) {
				solution.setSolution_description(solution
						.getSolution_description().trim());
			}
		}

		try {
			solutionService.replaceSolution(solution);
		} catch (Exception e) {
			log.error("更新解决方案数据异常", e);
			return "error";
		}

		isInsert = false;

		bubbleMsg = "ok";

		return "success";
	}

	/**
	 * 删除解决方案类型
	 * 
	 * @return 带有标志位的输出流
	 */
	public void removeSolutionByAjax() {

		HttpServletResponse resp = ServletActionContext.getResponse();
		PrintWriter out = null;
		String msg = "";

		try {
			Problem problem = new Problem();

			problem.setSolution(solution.getSolution_id());

			Integer count = problemService.searchCount(problem);

			if (count.equals(new Integer(0))) {
				solutionService.removeSolution(solution);
				msg = "删除成功";
			} else {
				solution = solutionService.getSolutionDetail(solution);
				msg = "仍有解决方案为" + solution.getSolution_name() + "的问题，无法删除该解决方案";
			}
		} catch (Exception e) {
			log.error("删除数据异常", e);
			msg = "删除数据异常";
		}

		try {
			out = resp.getWriter();
			out.write(msg);
			out.flush();
			out.close();
		} catch (IOException e) {
			log.error("输出流处理异常", e);
		} finally {
			if (null != out) {
				out.close();
			}
		}
	}

	/**
	 * 页面上显示等字段转换
	 * 
	 * @param temp
	 * @return
	 */
	private void changeToShow(Solution temp) {
		temp.setSolution_update_time_to_show(TimeUtil.removeMillionSecond(temp
				.getSolution_update_time()));

		temp.setSolution_description_to_show(TextUtil.stringEllipsis(temp
				.getSolution_description()));
	}

	/**
	 * @return the solutionService
	 */
	public SolutionService getSolutionService() {
		return solutionService;
	}

	/**
	 * @param solutionService
	 *            the solutionService to set
	 */
	public void setSolutionService(SolutionService solutionService) {
		this.solutionService = solutionService;
	}

	/**
	 * @return the solutionList
	 */
	public List<Solution> getSolutionList() {
		return solutionList;
	}

	/**
	 * @param solutionList
	 *            the solutionList to set
	 */
	public void setSolutionList(List<Solution> solutionList) {
		this.solutionList = solutionList;
	}

	/**
	 * @return the solution
	 */
	public Solution getSolution() {
		return solution;
	}

	/**
	 * @param solution
	 *            the solution to set
	 */
	public void setSolution(Solution solution) {
		this.solution = solution;
	}

	/**
	 * @return the problemService
	 */
	public ProblemService getProblemService() {
		return problemService;
	}

	/**
	 * @param problemService
	 *            the problemService to set
	 */
	public void setProblemService(ProblemService problemService) {
		this.problemService = problemService;
	}

	/**
	 * @return the isInsert
	 */
	public Boolean getIsInsert() {
		return isInsert;
	}

	/**
	 * @param isInsert
	 *            the isInsert to set
	 */
	public void setIsInsert(Boolean isInsert) {
		this.isInsert = isInsert;
	}

	/**
	 * @return the bubbleMsg
	 */
	public String getBubbleMsg() {
		return bubbleMsg;
	}

	/**
	 * @param bubbleMsg
	 *            the bubbleMsg to set
	 */
	public void setBubbleMsg(String bubbleMsg) {
		this.bubbleMsg = bubbleMsg;
	}

}
