package com.neusoft.zjop.action.main;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.neusoft.zjop.action.base.PageAction;
import com.neusoft.zjop.domain.Problem_Status;
import com.neusoft.zjop.domain.Statistics;
import com.neusoft.zjop.service.problem.ProblemService;
import com.neusoft.zjop.service.status.StatusService;

public class MainPageAction extends PageAction {
	private static final long serialVersionUID = 3602086052195894741L;

	private Logger log = Logger.getLogger(MainPageAction.class);

	private ProblemService problemService;

	private StatusService statusService;

	private String statusListJson;

	private String countListJson;

	public String toMainPage() {

		List<Statistics> countFinal = new ArrayList<Statistics>();

		List<Problem_Status> statusList = new ArrayList<Problem_Status>();

		List<String> statusNames = new ArrayList<String>();

		try {
			statusList = problemService.getProblemStatus();

			for (int i = 0; i < statusList.size(); i++) {

				Statistics col = new Statistics();

				Problem_Status temp = statusList.get(i);

				Integer count = statusService.getStatusCount(temp);

				col.setName(temp.getStatus_name());

				col.setY(count);

				col.setColor(Statistics.colors[i]);

				countFinal.add(col);

				statusNames.add(temp.getStatus_name());
			}
			
			log.info("主页状态列表柱状图加载成功");

		} catch (Exception e) {
			log.error("状态列表查询异常", e);
			return "error";
		}

		Gson gson = new Gson();

		countListJson = gson.toJson(countFinal);

		statusListJson = gson.toJson(statusNames);

		return "success";
	}

	/**
	 * @return the problemService
	 */
	public ProblemService getProblemService() {
		return problemService;
	}

	/**
	 * @param problemService
	 *            the problemService to set
	 */
	public void setProblemService(ProblemService problemService) {
		this.problemService = problemService;
	}

	/**
	 * @return the statusService
	 */
	public StatusService getStatusService() {
		return statusService;
	}

	/**
	 * @param statusService
	 *            the statusService to set
	 */
	public void setStatusService(StatusService statusService) {
		this.statusService = statusService;
	}

	/**
	 * @return the statusListJson
	 */
	public String getStatusListJson() {
		return statusListJson;
	}

	/**
	 * @param statusListJson
	 *            the statusListJson to set
	 */
	public void setStatusListJson(String statusListJson) {
		this.statusListJson = statusListJson;
	}

	/**
	 * @return the countListJson
	 */
	public String getCountListJson() {
		return countListJson;
	}

	/**
	 * @param countListJson
	 *            the countListJson to set
	 */
	public void setCountListJson(String countListJson) {
		this.countListJson = countListJson;
	}

}
