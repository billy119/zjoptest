package com.neusoft.zjop.action.base;

import org.apache.log4j.Logger;

import com.neusoft.zjop.action.page.PageHelper;
import com.neusoft.zjop.action.page.Page;
import com.opensymphony.xwork2.ActionSupport;

/**
 * <b>Application describing: 提供分页方法的基本父类，要分页的Action必须继承</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * 
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-5-5 上午09:40:48
 */
public class PageAction extends ActionSupport {

	private static final long serialVersionUID = -6850394813445369892L;

	private Logger log = Logger.getLogger(PageAction.class);

	private int page = 1;

	private int pageSize = Page.DEFAULT_PAGE_SIZE;

	private String url;

	private String param;

	private String pageBar;

	/**
	 * 分页方法
	 * 
	 * @param count
	 *            进行分页的条目数
	 * @return 分页对象：用于分页查询时取得页数以及大小
	 */
	protected final Page getPage(int count) {
		Page pageObj = new Page(getPage(), count, getPageSize(), getUrl(),
				getParam());
		this.pageBar = PageHelper.getPageBar(pageObj);
		return pageObj;
	}

	/**
	 * @return the log
	 */
	public Logger getLog() {
		return log;
	}

	/**
	 * @param log
	 *            the log to set
	 */
	public void setLog(Logger log) {
		this.log = log;
	}

	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page
	 *            the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize
	 *            the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the param
	 */
	public String getParam() {
		return param;
	}

	/**
	 * @param param
	 *            the param to set
	 */
	public void setParam(String param) {
		this.param = param;
	}

	/**
	 * @return the pageBar
	 */
	public String getPageBar() {
		return pageBar;
	}

	/**
	 * @param pageBar
	 *            the pageBar to set
	 */
	public void setPageBar(String pageBar) {
		this.pageBar = pageBar;
	}

}
