package com.neusoft.zjop.action.statistics;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.neusoft.zjop.domain.Fault;
import com.neusoft.zjop.domain.Problem;
import com.neusoft.zjop.domain.Statistics;
import com.neusoft.zjop.service.problem.ProblemService;
import com.neusoft.zjop.service.statistics.StatisticsService;
import com.neusoft.zjop.service.util.TimeUtil;

/**
 * <b>Application describing: 生成统计图标Action</b> <br>
 * 
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-5-14 上午10:09:11
 */
public class StatisticsAction {

	private static final long serialVersionUID = 153755926804342054L;

	private Logger log = Logger
			.getLogger("com.neusoft.zjop.action.statistics.StatisticsAction");

	private Problem problem;// 记录查询条件

	private List<Fault> faultList;// 维度

	private List<String> dateArray;// 日期List

	private String dateArrayJson;// 日期List Json

	private String resultListJson;// 数据List Json
	
	private String start_time;// 用来接收前台参数
	
	private String end_time;

	private ProblemService problemService;

	private StatisticsService statisticsService;

	@SuppressWarnings("deprecation")
	public String getData() {

		Map<String, List<Integer>> resultMap = new HashMap<String, List<Integer>>();
		if (null == problem) {														//若查询条件为空，初始化条件
			problem = new Problem();
			problem.setStart_time(TimeUtil.oneMonthAgoTimeStamp());					//开始时间默认为一个月前
			problem.setEnd_time(TimeUtil.nextOclock(TimeUtil.zeroOclock()));		//结束时间默认为现在
		}
		else {
																					//转换时间格式
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			problem.setStart_time(Timestamp.valueOf(df.format(start_time)));
			
			problem.setEnd_time(TimeUtil.nextOclock(Timestamp.valueOf(df.format(end_time))));		//若有查询条件，则将结束时间改为那天的最后一秒
			
		}
		problem.setStart_time_to_show(TimeUtil.removeMillionSecond(problem			//时间展示处理：去掉毫秒
				.getStart_time()));
		problem.setEnd_time_to_show(TimeUtil.removeMillionSecond(problem
				.getEnd_time()));
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		if (null == dateArray) {
			dateArray = new ArrayList<String>();
		}
																					//生成查询条件时间段的日期字符串List  以与查出来的问题个数的时间对应
		for (Timestamp i = (Timestamp) problem.getStart_time().clone(); i.before(problem.getEnd_time())||i.equals(problem.getEnd_time());
		i.setDate(i.getDate() + 1)) {
			dateArray.add(df.format(i));
		}

		try {
			faultList = problemService.getFaults();									//查询故障类型，以遍历

			for (Iterator<Fault> i = faultList.iterator(); i.hasNext();) {

				List<Integer> problemCounts = new ArrayList<Integer>();

				Fault faultIt = (Fault) i.next();

				problem.setFault_type(faultIt.getFault_id());

				List<Map<String, String>> temp = statisticsService					//查询出来的按日期分组的问题个数Map: <日期(yyyy-MM-dd),个数>
						.getData(problem);

				for (Iterator<?> it = dateArray.iterator(); it.hasNext();) {		//遍历日期List,将查出来的Map与之对照 没有的日期用0填充
					String date = (String) it.next();
					boolean ishave = false;											//该日期是否有问题的标志位
					for (Iterator<Map<String, String>> itt = temp.iterator(); itt	//遍历查出来的Map List List以日期为度量
							.hasNext();) {
						Map<String, String> tt = (Map<String, String>) itt
								.next();

						if (date.equals(tt.get("dateTemp"))) {						//判断List的该元素的日期是否是日期List的日期
							problemCounts.add(Integer.valueOf(tt					//如果是，则将查出来的MapList的问题个数值取得
									.get("problemCount")));
							ishave = true;											//并置标志位
						}

					}

					if (!ishave) {													//如果查出来的MapList没有时间List里的日期的元素，说明在该日期上没有问题
						problemCounts.add(0);										//填充0
					}

				}

				resultMap.put(faultIt.getFault_name(), problemCounts);				//将该故障下的问题个数List加入到最终Map中
			}

			log.info("查询统计数据成功");
			
		} catch (Exception e) {
			log.error("查询统计数据异常", e);
			return "error";
		}

		List<Statistics> resultList = new ArrayList<Statistics>();

		for (Iterator<Fault> i = faultList.iterator(); i.hasNext();) {				//封装至Statistics对象中，以对应HighCharts控件的数据格式
			Fault fault = (Fault) i.next();
			Statistics st = new Statistics();
			st.setName(fault.getFault_name());
			st.setData(resultMap.get(st.getName()));
			resultList.add(st);
		}

		Gson gson = new Gson();

		resultListJson = gson.toJson(resultList);									//转换为json

		List<String> d = new ArrayList<String>();
		
		for (Iterator<String> i = dateArray.iterator(); i.hasNext();) {				//图标横坐标日期格式转换
			String dateTemp = (String) i.next();
			String temp = dateTemp.substring(5, dateTemp.length()).replace('-', '/');
			temp = temp.charAt(3)=='0'?temp.substring(0,3)+temp.substring(4):temp;
			d.add(temp.startsWith("0")?temp.substring(1):temp);
		}
		
		dateArrayJson = gson.toJson(d);

		return "success";
	}

	/**
	 * @return the problem
	 */
	public Problem getProblem() {
		return problem;
	}

	/**
	 * @param problem
	 *            the problem to set
	 */
	public void setProblem(Problem problem) {
		this.problem = problem;
	}

	/**
	 * @return the problemService
	 */
	public ProblemService getProblemService() {
		return problemService;
	}

	/**
	 * @param problemService
	 *            the problemService to set
	 */
	public void setProblemService(ProblemService problemService) {
		this.problemService = problemService;
	}

	/**
	 * @return the faultList
	 */
	public List<Fault> getFaultList() {
		return faultList;
	}

	/**
	 * @param faultList
	 *            the faultList to set
	 */
	public void setFaultList(List<Fault> faultList) {
		this.faultList = faultList;
	}

	/**
	 * @return the statisticsService
	 */
	public StatisticsService getStatisticsService() {
		return statisticsService;
	}

	/**
	 * @param statisticsService
	 *            the statisticsService to set
	 */
	public void setStatisticsService(StatisticsService statisticsService) {
		this.statisticsService = statisticsService;
	}

	/**
	 * @return the dateArray
	 */
	public List<String> getDateArray() {
		return dateArray;
	}

	/**
	 * @param dateArray
	 *            the dateArray to set
	 */
	public void setDateArray(List<String> dateArray) {
		this.dateArray = dateArray;
	}

	/**
	 * @return the resultListJson
	 */
	public String getResultListJson() {
		return resultListJson;
	}

	/**
	 * @param resultListJson
	 *            the resultListJson to set
	 */
	public void setResultListJson(String resultListJson) {
		this.resultListJson = resultListJson;
	}

	/**
	 * @return the dateArrayJson
	 */
	public String getDateArrayJson() {
		return dateArrayJson;
	}

	/**
	 * @param dateArrayJson
	 *            the dateArrayJson to set
	 */
	public void setDateArrayJson(String dateArrayJson) {
		this.dateArrayJson = dateArrayJson;
	}

	/**
	 * @return the start_time
	 */
	public String getStart_time() {
		return start_time;
	}

	/**
	 * @param start_time the start_time to set
	 */
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	/**
	 * @return the end_time
	 */
	public String getEnd_time() {
		return end_time;
	}

	/**
	 * @param end_time the end_time to set
	 */
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

}
