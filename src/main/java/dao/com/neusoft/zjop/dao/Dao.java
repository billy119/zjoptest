package com.neusoft.zjop.dao;

import java.sql.SQLException;
import java.util.List;

/**
 * <b>Application describing:操作数据库Dao接口 </b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-4-27 下午01:23:36
 */
public interface Dao 
{
	/**
	 * 获取对象聚集
	 * @param statement
	 * @param bussiness
	 * @return
	 * @throws SQLException
	 */
	List<?> getObjects(String statement , Object bussiness) throws SQLException;
	
	/**
	 * 获取单个对象
	 * @param statement
	 * @param bussiness
	 * @return
	 * @throws SQLException
	 */
	Object getSingleObject(String statement , Object bussiness) throws SQLException;
	
	/**
	 * 插入单个对象
	 * @param statement
	 * @param bussiness
	 * @return 插入表的对应主键
	 * @throws SQLException
	 */
	Object insertSingleObject(String statement , Object bussiness) throws SQLException;
	
	/**
	 * 更新单个对象
	 * @param statement
	 * @param bussiness
	 * @return 影响的记录个数
	 * @throws SQLException
	 */
	int updateSingleObject(String statement , Object bussiness) throws SQLException;
	
	/**
	 * 删除单个对象
	 * @param statement
	 * @param bussiness
	 * @return 影响的记录个数
	 * @throws SQLException
	 */
	int deleteSingleObject(String statement , Object bussiness) throws SQLException;
	
	/**
	 * 分页获取对象聚集
	 * @param statement 查询语句
	 * @param obj 查询条件对象
	 * @param skipResults 当页的开始记录
	 * @param maxResults 每页条数
	 * @return
	 * @throws SQLException
	 */
	List<?> getObjectsByPage(String statement, Object bussiness, int skipResults, int maxResults) throws SQLException;
}
