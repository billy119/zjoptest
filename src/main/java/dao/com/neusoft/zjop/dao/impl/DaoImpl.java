package com.neusoft.zjop.dao.impl;

import java.sql.SQLException;
import java.util.List;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.neusoft.zjop.dao.Dao;

/**
 * 
 * <b>Application describing: ibatis Dao实现类</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-4-27 下午01:37:13
 */
public class DaoImpl extends SqlMapClientDaoSupport implements Dao {

	@Override
	public int deleteSingleObject(String statement, Object bussiness)
			throws SQLException {
		return getSqlMapClientTemplate().delete(statement, bussiness);
	}

	@Override
	public List<?> getObjects(String statement, Object bussiness)
			throws SQLException {
		return getSqlMapClientTemplate().queryForList(statement, bussiness);
	}

	@Override
	public List<?> getObjectsByPage(String statement, Object bussiness,
			int skipResults, int maxResults) throws SQLException {
		return getSqlMapClientTemplate().queryForList(statement, bussiness,
				skipResults, maxResults);
	}

	@Override
	public Object getSingleObject(String statement, Object bussiness)
			throws SQLException {
		return getSqlMapClientTemplate().queryForObject(statement, bussiness);
	}

	@Override
	public Object insertSingleObject(String statement, Object bussiness)
			throws SQLException {
		return getSqlMapClientTemplate().insert(statement, bussiness);
	}

	@Override
	public int updateSingleObject(String statement, Object bussiness)
			throws SQLException {
		return getSqlMapClientTemplate().update(statement, bussiness);
	}

}
