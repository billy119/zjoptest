package com.neusoft.zjop.domain;

import java.util.List;

/**
 * <b>Application describing: 问题统计实体类</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * 
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-5-13 上午10:43:43
 */
public class Statistics {

	public static final String[] colors = { "#DEB887", "#FF6666", "#98FB98",
			"#7bd5f3" };

	private String name; // 故障名称（维度）

	private List<Integer> data; // 每日问题个数的数组

	private Integer y; // 单个数据的数据值

	private String color; // 颜色

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the data
	 */
	public List<Integer> getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(List<Integer> data) {
		this.data = data;
	}

	/**
	 * @return the y
	 */
	public Integer getY() {
		return y;
	}

	/**
	 * @param y
	 *            the y to set
	 */
	public void setY(Integer y) {
		this.y = y;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color
	 *            the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

}
