package com.neusoft.zjop.domain;

/**
 * <b>Application describing: 问题状态域</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * 
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-4-28 上午09:02:06
 */
public class Problem_Status {

	private Integer status_id; // 状态ID

	private String status_name; // 状态名称

	/**
	 * @return the status_id
	 */
	public Integer getStatus_id() {
		return status_id;
	}

	/**
	 * @param status_id
	 *            the status_id to set
	 */
	public void setStatus_id(Integer status_id) {
		this.status_id = status_id;
	}

	/**
	 * @return the status_name
	 */
	public String getStatus_name() {
		return status_name;
	}

	/**
	 * @param status_name
	 *            the status_name to set
	 */
	public void setStatus_name(String status_name) {
		this.status_name = status_name;
	}

}
