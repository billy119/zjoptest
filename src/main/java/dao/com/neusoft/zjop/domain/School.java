package com.neusoft.zjop.domain;

/**
 * 
 * <b>Application describing: 从其他系统获得的学校实体类</b> <br>
 * <b>Copyright:</b>Copyright &copy; 2014 东软 移动互联网事业部版权所有。<br>
 * <b>Company:</b>Neusoft<br>
 * 
 * @author <a href="mailto:li-zr@neusoft.com">Zongrui.Li </a>
 * @version Revision 1.0 2014-4-29 上午11:07:19
 */
public class School {

	/**
	 * 组织结构ID （学校ID）
	 */
	private String dept_id;

	/**
	 * 机构名称 （学校名称）
	 */
	private String dept_name;

	/**
	 * @return the dept_id
	 */
	public String getDept_id() {
		return dept_id;
	}

	/**
	 * @param dept_id
	 *            the dept_id to set
	 */
	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}

	/**
	 * @return the dept_name
	 */
	public String getDept_name() {
		return dept_name;
	}

	/**
	 * @param dept_name
	 *            the dept_name to set
	 */
	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}

}
