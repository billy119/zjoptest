<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- javaScript嵌入 -->
<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/validate.js"></script>
<script type="text/javascript">
	var zj = {};
	zj.showSystemMessage = function (msg,width1,bgc,time){
		bgc&&$("#opTips_save").css({backgroundColor:bgc});
		$("#opTips_save").css({display:'inline-block'});
		$("#opTips_save").css({height:'30px'});
		$("#opTips_save").css({width:width1});
		$("#opTips_save").html(msg);
	 		 var m;
			 var c = function(){
		  var element = document.getElementById("opTips_save"); 
		  if(element){
			  with(element.style)   
			  {
			  if(parseInt(height)){
					height = parseInt(height) - 1 + "px";
					if (parseInt(height) < 1){
					  clearInterval(m);
					 $("#opTips_save").css({backgroundColor:'#efc958'});
					 element.style.display = 'none';
					}
			   }
			  }
		  	}
 		  }
 		  time && setTimeout(function(){m = setInterval(c,1);},time)||setTimeout(function(){m = setInterval(c,1);},1500);
 		 
	};
	
	$(document).ready(function(){
	
		var msg = '<s:property value="bubbleMsg"/>';
		
		if('ok' == msg){
			zj.showSystemMessage('<s:text name="problem.global.saveComplete" />');
		}
	});
</script>
<!-- css嵌入 -->
<link href="style/css/common.css" rel="stylesheet">
<title>诸暨教育云问题管理系统</title>
<decorator:head />

</head>

<body
	style="margin: 0; padding: 0; border: none; color: #666; font: 14px/ 18px Arial, Verdana, '宋体', '微软雅黑'; background-color: #f3f4f4;">
<!--屏幕上方的提示信息-->
<div id="opTips_save"
	style="line-height:30px;text-align:center;font-size: 14px; color: #fff; font-weight: bold;padding: 0px 5px 0px 5px;z-index: 999;width:320px; height: 30px; background-image: -moz-linear-gradient(top, #41B1F2, #517FB3);
	background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #41B1F2), color-stop(1, #517FB3)); 
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#41B1F2', endColorstr='#517FB3', GradientType='0'); 
 	position: fixed; top: 0px;left:0px;right:0px;margin-left:auto;margin-right:auto; display: none; border-radius: 5px 5px 5px 5px;border: 1px solid #cccccc;">
	</div>
<div style="width: 800px; height: auto;"><decorator:body /></div>

</body>
</html>