/**
 * 问题页面(新增、编辑)校验
 */
function validate()
{
	/*上报人*/
	
	/*学校*/
	if (document.getElementById("school")&&!document.getElementById("school").value) {
		zj.showSystemMessage("请填写学校");
		return false;
	}
	/*故障类型*/
	if (document.getElementById("fault")&&!document.getElementById("fault").value) {
		zj.showSystemMessage("请选择故障类型");
		return false;
	}
	/*处理方案*/
	if (document.getElementById("solution")&&"none"!=$("#solutionLi").css("display")) {
		var statusArr = eval($("#statusList").val());					/*状态List*/
		var index = 0;											/*已解决的下标记录*/
		for(;index < statusArr.length ; index++){				/*找到已解决的下标*/
			if("已解决" == statusArr[index].status_name)
				break;
		}
																/*与当前问题状态进行比较*/
		if ($("#problemStatus").val()&&parseInt($("#problemStatus").val())>=index) {
			if(!document.getElementById("solution").value){		/*如果当前状态在已解决之后，则进行校验*/
				zj.showSystemMessage("请选择解决方案");
				return false;
			}
		} 
	}
	
	/*故障类型(故障页面)*/
	if (document.getElementById("fault_name")&&!document.getElementById("fault_name").value) {
		zj.showSystemMessage("请填写故障类型");
		return false;
	}
	
	/*时间先后顺序*/
	if (document.getElementById("start_time")&&document.getElementById("end_time")
	&&document.getElementById("start_time").value>document.getElementById("end_time").value) {
		zj.showSystemMessage("开始时间应早于结束时间");
		return false;
	}

	return true;
}

/**
 * 获取客户端当前时间
 */
function CurrentTime()
{ 
    var now = new Date();
   
    var year = now.getFullYear();       //年
    var month = now.getMonth() + 1;     //月
    var day = now.getDate();            //日
   
    var hh = now.getHours();            //时
    var mm = now.getMinutes();          //分
    var s = now.getSeconds();			//秒
   
    var clock = year + "-";
   
    if(month < 10)
        clock += "0";
   
    clock += month + "-";
   
    if(day < 10)
        clock += "0";
       
    clock += day + " ";
   
    if(hh < 10)
        clock += "0";
       
    clock += hh + ":";
    if (mm < 10) clock += '0'; 
    clock += mm + ":"; 
    if (s < 10) clock += '0';
    clock += s;
    return(clock); 
} 