<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="style/css/ZJ/faultEdit.css" rel="stylesheet" />
<script type="text/javascript">
	/*全局变量isEdit 表示当前状态是浏览状态还是编辑状态*/
	var isEdit = false;

	$(document).ready(function(){
		var isInsert = $("#isInsert").val();
		if(isInsert == 'true'){
			isEdit = true;
			formReadonly(false);
		}
	});
        
	/*编辑/保存按钮点击事件*/
	function buttonChange(){
		if(isEdit&&validate()){								//当前状态为编辑状态：点击事件为保存更改
			$("#editForm").submit();
		}
		else{									//当前状态为浏览状态：点击事件为改变按钮文字为“编辑”，将页面变为可编辑
			formReadonly(false);
			isEdit = true;
		}
	}
	
	/*表单只读*/
	function formReadonly(flag){
		if(flag){
			$("#editButton").html('<s:text name="problem.button.edit"/>');
			$("textarea").attr("readonly","readonly");
			$("input:text:not(#reportTime)").attr("readonly","readonly");
		}
		else{
			$("#editButton").html('<s:text name="problem.button.save"/>');
			$("textarea").removeAttr("readonly");
			$("input:text:not(#reportTime)").removeAttr("readonly");
		}
	}
</script>
</head>
<body>

<div id="editDiv">
	<s:form action="replaceFault.action" id="editForm">
	<ul>
	<!-- 故障类型 -->
	<li>
		<input type="hidden" name="fault.fault_id" value="${fault.fault_id}">
		<input type="hidden" id="isInsert" name="isInsert" value="${isInsert}">
		<span><s:text name="fault.edit.faultType"/></span>
		<input type="text" id="fault_name" name="fault.fault_name" value="${fault.fault_name}" readonly="readonly"/>
	</li>
	<!-- 故障类型描述 -->
	<li class="wholeLine" style="height:auto;">
		<span><s:text name="fault.edit.faultDescription"/></span>
	</li>
	<li class="wholeLine" style="height:auto;margin-top: 0px;">
		<textarea name="fault.fault_description" style="width:320px;height:60px;" readonly="readonly">${fault.fault_description}</textarea>
	</li>
	</ul>
	</s:form>
</div>

<div id="buttonDiv" class="buttonImg" style="padding-left: 200px;margin-left: 0px;">
	<ul>
		<!-- 编辑/保存 -->
		<li>
			<a id="editButton" onclick="buttonChange()"><s:text name="fault.edit.edit"/></a>
		</li>
		<!-- 返回 -->
		<li>
			<a onclick="location.href='./faultManagement.action?page=' + ${page} + '&pageSize=' + ${pageSize}"><s:text name="fault.edit.return"/></a>
		</li>
	</ul>
</div>

</body>
</html>