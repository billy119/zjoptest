<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="style/css/ZJ/faultQuery.css" rel="stylesheet" />

<script type="text/javascript">
	/*删除故障*/
	function removeSolution(obj){
		/*获取故障id*/
		var tr=obj.parentNode.parentNode;
		var tbody=tr.parentNode;
		var guid=tr.children[0].children[0].value;
		if(window.confirm('<s:text name="solution.query.deleteConfirm"/>')){
			$.ajax({
				type:"post",
				url:"removeSolutionByAjax.action",
				data: {"solution.solution_id" : guid},
 				success:function(str)
				{
					zj.showSystemMessage(str);
					if("删除成功"==str)
						tbody.removeChild(tr); 
				},
				error:function(str)
				{
					zj.showSystemMessage(str);
				}
		});
		}
	}
	
	/*切换选项，更换页面*/
	function changeType(){
		location.href = './faultManagement.action';
	}
</script>

</head>
<body>
<!-- 操作按钮button DIV -->
<div id="buttonDiv">
	<ul>
		<li>
			<SPAN><s:text name="solution.query.choice" /></SPAN>
			<select id="conf" onchange="changeType();">
				<option value="1"><s:text name="fault.query.choice.fault" /></option>
				<option value="2" selected="selected"><s:text name="fault.query.choice.solution" /></option>
			</select>
		</li>
		<li style="width: 150px;">
			<a class="btn2" onclick="location.href = './getSingleSolution.action';"><s:text name="solution.query.add" /></a>
		</li>
		<li style="width: 150px;">
			<a class="btn2" onclick="location.href = './';"><s:text name="solution.query.return" /></a>
		</li>
	</ul>
</div>

<!-- 查询结果table DIV -->
<div id="resultTableDiv">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th width="24%"><s:text name="solution.query.updateTime" /></th>
			<th width="15%"><s:text name="solution.query.type"/></th>
			<th width="50%"><s:text name="solution.query.description"/></th>
			<th><s:text name="solution.query.operation"/></th>
		</tr>
		<s:iterator value="solutionList" status="st">
	        <tr <s:if test="#st.odd == true">class="trBg0" onmouseover="this.className='trOver'"  onmouseout="this.className='trBg0'"</s:if>
	           <s:else>class="trBg1" onmouseover="this.className='trOver'" onmouseout="this.className='trBg1'"</s:else>>
		        <td><INPUT type="hidden" value="<s:property value='solution_id'/>" /><s:property value="solution_update_time_to_show"/></td>
		        <td><s:property value="solution_name"/></td>
		        <td title="<s:property value="solution_description"/>"><s:property value="solution_description_to_show"/></td>
		        <td>
		        	<img style="cursor: pointer;width: 20px;margin-right: 5px;" title="<s:text name="problem.table.edit"/>" alt="<s:text name="problem.table.edit"/>" src="style/image/edit.png" onclick="window.location='./getSingleSolution.action?solution.solution_id='+'<s:property value="solution_id"/>' + '&page=' + ${page} + '&pageSize=' + ${pageSize}">
		        	<img style="cursor: pointer;width: 20px;margin-right: 5px;" title="<s:text name="problem.table.delete"/>" alt="<s:text name="problem.table.delete"/>" src="style/image/delete.png" onclick="removeSolution(this);">
		        </td>
  			</tr>
		</s:iterator>
	
	</table>
	<s:property value="pageBar" escape="false" />
</div>
</body>
</html>