<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="js/graphs/highcharts.js"></script>
<script type="text/javascript" src="js/graphs/exporting.js"></script>
<script type="text/javascript" src="js/DateTimeCalendar.js"></script>
<link href="style/css/ZJ/statistics.css" rel="stylesheet" />
<script type="text/javascript">
	$(function () {
	        $('#container').highcharts({
	            title: {
	                text: '故障类型统计', // 标题
	                x: -20 //center
	            },
	            subtitle: {
	                text: "<s:property value='problem.start_time_to_show'/>" + ' ~ ' + "<s:property value='problem.end_time_to_show'/>",
	                x: -20
	            },
	            xAxis: {							//横坐标数组
	                categories: ${dateArrayJson}
	            },
	            yAxis: {
	                title: {
	                    text: '问题个数'	//纵坐标意义
	                },
	                plotLines: [{
	                    value: 0,
	                    width: 1,
	                    color: '#808080'
	                }]
	            },
	            tooltip: {
	                formatter: function () {  
                            return '<b>' + this.x.replace('/','月') + '日</b><br/>'
                            + this.series.name + ':  ' + this.y + '个';
                        }
	            },
	            legend: {
	                layout: 'vertical',
	                align: 'right',
	                verticalAlign: 'middle',
	                borderWidth: 0
	            },
	            series: ${resultListJson}
	        });
	    });
	    
    function timeValidate(){
    	/*时间先后顺序*/
		if (document.getElementById("start_time")&&document.getElementById("end_time")
		&&document.getElementById("start_time").value>document.getElementById("end_time").value) {
			zj.showSystemMessage("开始时间应早于结束时间");
			return false;
		}
		return true;
    }
	</script>
</head>
<body>

<div id="editDiv"><s:form action="graph.action"
	id="editForm">
	<ul>
		<!-- 开始时间 -->
		<li><span><s:text name="problem.query.startTime" /></span> <input
			type="text" id="start_time" name="start_time" onfocus="calendar();"
			style="cursor: pointer;" readonly="readonly" value="${problem.start_time_to_show}" /></li>
		<!-- 结束时间 -->
		<li><span><s:text name="problem.query.endTime" /></span> <input
			type="text" id="end_time" name="end_time" onfocus="calendar();"
			style="cursor: pointer;" readonly="readonly" value="${problem.end_time_to_show}" /></li>
		<li style="width: 340px;">
			<a class="btn2" onclick="if(timeValidate())$('#editForm').submit();"><s:text name="problem.button.query"/></a>
			<a class="btn2" onclick="location.href = './';"><s:text name="problem.button.return"/></a>
		</li>
	</ul>
</s:form></div>
<div id="container"
	style="min-width: 310px; height: 400px; margin: 0 auto"></div>
</body>
</html>