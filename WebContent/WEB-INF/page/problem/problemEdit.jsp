<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="style/css/ZJ/problemEdit.css" rel="stylesheet" />
<link href="style/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery-ui-1.10.4.custom.js"></script>
<script type="text/javascript">
	/*全局变量isEdit 表示当前状态是浏览状态还是编辑状态*/
	var isEdit = false;
	/*全局变量statusList 表示状态List的json字符串*/
	var statusList = ${statusJson};
	
	$(document).ready(function(){
		initStatusButton();
		formReadonly(true);
	});

	 /*学校输入框autocomplete*/
	 $(function(){
            $("#school").autocomplete({
                minLength:1,
                autoFocus:true,
                source:function(request,response){
                    $.ajax({
                        type:"POST",
                        url:"getSchool.action",
                        dataType:"json",
                        data:{ "school.dept_name" :  $("#school").val() },
                        success:function(json){
                            response($.map(json,function(item){
                                return {
                                    label:item.dept_name,
                                    value:item.dept_name
                                };
                            }));
                        }
                    });
                }
            });
        });
        
	/*提交表单时Ajax校验*/
    function schoolValidate(){
    	
    	var bol;
    
    	$.ajax({
            type:"POST",
            async: false,
            url:"validateSchool.action",
            dataType:"json",
            data:{ "school.dept_name" :  $("#school").val() },
            success:function(json){
                if(!json){
                	zj.showSystemMessage("请输入正确的学校");
                	$("#school").focus();
                	bol=false;
                }
                else
               	 	bol=true;
            }
        });
        
        return bol;
    }
        
	/*编辑/保存按钮点击事件*/
	function buttonChange(){
	
		var vaSc = schoolValidate();
		if(isEdit){
			if(validate()&&vaSc){
				if(isEdit&&saveProblem()){								//当前状态为编辑状态：点击事件为通过Ajax保存更改
					formReadonly(true);
					isEdit = false;
				}
			}
		}
		else{									//当前状态为浏览状态：点击事件为改变按钮文字为“编辑”，将页面变为可编辑
			formReadonly(false);
			isEdit = true;
		}
	}
	
	/*校验成功后ajax保存问题数据*/
    function saveProblem(){
   		var form = $("#editForm").serialize();
		$.ajax({
			type:"post",
			url:"replaceProblemByAjax.action",
			data: form,
			success:function(str)
			{
				var sysMsg = str.split(',')[0];
				zj.showSystemMessage(sysMsg);
				var reportPerson = str.split(',')[1];
				if(reportPerson)
					$("#reportPerson").val(reportPerson);
			},
			error:function(str)
			{
				zj.showSystemMessage(str);
				return false;
			}
		   });
		return true;
    }
	
	/*表单只读*/
	function formReadonly(flag){
		if(flag){
			$("#editButton").html('<s:text name="problem.button.edit"/>');
			$("textarea").attr("readonly","readonly");
			$("input:text:not(#reportTime)").attr("readonly","readonly");
			$("select").css("display","none");									/*下拉框不显示*/
			if($("#problemStatus").val() != statusList[statusList.length - 1].status_id)/*如果是已关闭状态，则不显示该按钮*/
				$("#statusChange").css("display","block");
			
																				/*下拉框对应的input显示*/
			$("#faultInput").css("display","block").val($("#fault option:selected").html());
			$("#solutionInput").css("display","block").val($("#solution option:selected").html());
		}
		else{
			$("#editButton").html('<s:text name="problem.button.save"/>');
			$("textarea").removeAttr("readonly");
			$("input:text:not(#reportTime)").removeAttr("readonly");
			$("select").css("display","block");									/*下拉框显示*/
																				/*下拉框对应的input消失*/
			$("#faultInput").css("display","none"); 								
			$("#solutionInput").css("display","none");
			$("#statusChange").css("display","none");							/*状态按钮不显示*/
			
			
			$("#solution")[0].options[0].innerHTML = "--  请选择  --";/*保存之后可以填写的提示*/
		}
	}
	
	/*问题处理流程*/
	function problemNextAction(){
	
		var vaSc = schoolValidate();
	
		if(validate()&&vaSc){
			var problemStatus = $("#problemStatus").val();
			
			for(var i = 0;i<statusList.length;i++){
				if(problemStatus == statusList[i].status_id){
					if(i == statusList.length - 1)				/*以防万一！*/
						return;
					$.ajax({									/*调用Ajax*/
					type:"post",
					url:"updateProblemsStatusByAjax.action",
					data: {"problem.problem_status" : statusList[i+1].status_id , "guids" : $("#problemGuid").val()},
	 				success:function(str)
					{																					
						zj.showSystemMessage(str);
						
						if("状态更新成功" == str){
							$("#problemStatus").val(statusList[parseInt(problemStatus)].status_id);
							initStatusButton();
							$("#problemStatusString").html('<s:text name="problem.edit.status"/>'+statusList[parseInt(problemStatus)].status_name);
						}
					},
					error:function(str)
					{
						zj.showSystemMessage(str);
					}
					});
																/*更改按钮显示*/
				}
			}
		}
	}
	
	/*初始化状态流程按钮显示*/
	function initStatusButton(){
		var problemStatus = $("#problemStatus").val();				
		
		if(problemStatus == statusList[statusList.length - 1].status_id){/*如果是已关闭状态，则不显示该按钮*/
			$("#statusChange").css("display","none");
			return;
		}
		
		for(var i = 0 ; i < statusList.length ; i++){				/*如果不是已关闭状态，则显示下一个状态的后两个字符*/
			if(problemStatus == statusList[i].status_id){	
				if(1 == problemStatus)											/*暂写死*/
					$("#statusChange").html("处理");
				else if(2 == problemStatus){
					$("#statusChange").html("解决");
																				/*显示解决方案和处理结果*/
					$("#solutionLi").css("display","block");
					$("#resultLi").css("display","block");
				}
				else if(3 == problemStatus){
					$("#statusChange").html("关闭");
					/*因为该页面保存之后跳转新的页面，因此处理中以及处理中以后的所有状态都需要显示解决方案和处理结果*/
					$("#solutionLi").css("display","block");
					$("#resultLi").css("display","block");
				}
				else {
					/*因为该页面保存之后跳转新的页面，因此处理中以及处理中以后的所有状态都需要显示解决方案和处理结果*/
					$("#solutionLi").css("display","block");
					$("#resultLi").css("display","block");
				}				
				/*$("#statusChange").html(statusList[i+1].status_name.substring(1,2)+"  "+statusList[i+1].status_name.substring(2,3));*/
			}
		}
	}
</script>
</head>
<body>

<div id="editDiv">
	<s:form action="replaceProblem.action" id="editForm">
	<ul>
	<!-- 问题状态显示(不能编辑) -->
	<li>
		<!-- 隐藏guid -->
		<input type="hidden" id="problemGuid" name="problem.guid" value="${problem.guid}">
		<!-- 是否返回新建问题页面的标志位 -->
		<input type="hidden" name="returnNew" value="false">
		<!-- 隐藏问题状态 -->
		<input type="hidden" id="problemStatus" name="problem.problem_status" value="${problem.problem_status}">
		<!-- 隐藏问题状态List -->
		<input type="hidden" id="statusList" value="${statusJson}">
		<span id="problemStatusString" style="width:200px;text-align:left;position:absolute;"><s:text name="problem.edit.status"/>${problem.problem_status_to_show}</span>
		<!-- 更改状态按钮 -->
		<a id="statusChange" style="height:26px;line-height:26px;width:56px;margin-right:100px;margin-top:2px;margin-bottom:2px;float: right;padding: 0px;position: relative;" class="btn2" onclick="problemNextAction();"></a>
	</li>
	<!-- 上报时间 -->
	<li>
		<span><s:text name="problem.edit.reportTime"/></span>
		<input id="reportTime" type="text" readonly="readonly" value="${problem.report_time_to_show}"/>
	</li>
	</ul>
	<div style="background-color: #FFFFFF;padding-top: 5px;padding-bottom: 5px;border: 1px solid #DCDCDC;">
	<ul>
	<!-- 上报人 -->
	<li>
		<span><s:text name="problem.edit.reportPerson"/></span>
		<input type="text" name="problem.report_person" title="<s:text name='problem.edit.title.reportPerson'/>" value="${problem.report_person}" readonly="readonly"/>
	</li>
	<!-- 学校 -->
	<li>
		<span><s:text name="problem.query.school"/></span>
		<input id="school" name="problem.school" type="text" value="${problem.school_to_show}" readonly="readonly"/>
	</li>
	<!-- 故障类型 -->	
	<li>
		<span><s:text name="problem.query.faultType"/></span>
		<s:select id="fault" name="problem.fault_type" list="faultList" listKey="fault_id" listValue="fault_name" headerKey="" headerValue="--  请选择  --" cssStyle="display: none;" ></s:select>
		<input type="text" readonly="readonly" id="faultInput" >
	</li>
	<!-- 处理方案 -->
	<li id="solutionLi" style="display:none;">
		<span><s:text name="problem.query.solution"/></span>
		<s:select id="solution" name="problem.solution" list="solutionList" listKey="solution_id" listValue="solution_name" headerKey="" headerValue="--  请选择  --" cssStyle="display: none;" ></s:select>
		<input type="text" readonly="readonly" id="solutionInput" >
	</li>
	<!-- 故障描述 -->
	<li class="wholeLine" style="height:auto;">
		<span><s:text name="problem.edit.faultDescription"/></span>
		<textarea name="problem.fault_description" style="width:300px;height:100px;" readonly="readonly">${problem.fault_description}</textarea>
	</li>
	<!-- 处理结果 -->
	<li id="resultLi" class="wholeLine" style="height:auto;display: none;">
		<span><s:text name="problem.query.result"/></span>
		<textarea name="problem.result" style="width:300px;height:100px;" readonly="readonly">${problem.result}</textarea>
	</li>
	</ul>
	</div>
	</s:form>
</div>

<div id="buttonDiv" class="buttonImg">
	<ul>
		<!-- 编辑/保存 -->
		<li>
			<a id="editButton" onclick="buttonChange()"><s:text name="problem.button.edit"/></a>
		</li>
		<!-- 返回 -->
		<li>
			<a onclick="location.href='./problemManagement.action?page=' + ${page} + '&pageSize=' + ${pageSize}"><s:text name="problem.button.return"/></a>
		</li>
	</ul>
</div>

</body>
</html>