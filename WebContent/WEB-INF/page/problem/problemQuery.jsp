<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="js/DateTimeCalendar.js"></script>
<link href="style/css/ZJ/problemQuery.css" rel="stylesheet" />
<link href="style/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery-ui-1.10.4.custom.js"></script>
<script type="text/javascript">

	var guidStr;

	 /*学校输入框autocomplete*/
	 $(function(){
            $("#school").autocomplete({
                minLength:1,
                autoFocus:true,
                source:function(request,response){
                    $.ajax({
                        type:"POST",
                        url:"getSchool.action",
                        dataType:"json",
                        data:{ "school.dept_name" :  $("#school").val() },
                        success:function(json){
                            response($.map(json,function(item){
                                return {
                                    label:item.dept_name,
                                    value:item.dept_name
                                };
                            }));
                        }
                    });
                }
            });
        });
	/*点击更改状态按钮*/
	function changeStatus(){
		var guids = new Array();
		if(0 == $(":checkbox:checked[name=box]").length){
			zj.showSystemMessage('<s:text name="problem.query.changeStatusWarn"/>');
			return false;
		}
		$(":checkbox:checked[name='box']").each(function(){
			guids.push($(this).val());
		});
		guidStr = guids.join(",");
		$("#statusChangeDiv").css("display","block");
	}
	
	/*批量更改问题状态Ajax*/
	function clickOkToChangeStatus(){
		var radio = $(":radio:checked[name='statusToChange']").val();
		
		if(!radio){
			zj.showSystemMessage('<s:text name="problem.query.selectRadioWarn"/>');
			return false;
		}
		
		$.ajax({
				type:"post",
				url:"updateProblemsStatusByAjax.action",
				data: {"problem.problem_status" : radio , "guids" : guidStr},
 				success:function(str)
				{																					
					zj.showSystemMessage(str);
					if("状态更新成功" == str){
						var text = $(":radio[name='statusToChange'][value='"+radio+"']")[0].parentNode.children[1].innerHTML;
						$(":checkbox:checked[name='box']").each(function(){
							this.parentNode.parentNode.children[6].innerHTML = text;
						});
					}
				},
				error:function(str)
				{
					zj.showSystemMessage(str);
				}
		});
		
		$("#statusChangeDiv").css("display","none");
	}
	
	/*全选(简陋)*/
	function selALl(obj){
		var checked = obj.checked;
		var boxes = document.getElementsByName('box');
		if(checked){
			for(var i = 0 ; i < boxes.length ; i++)
				boxes[i].checked = true;
		}
		else{
			for(var i = 0 ; i < boxes.length ; i++)
				boxes[i].checked = false;
		}
	}
	
	/*删除问题*/
	function removeProblem(obj){
		/*获取问题的guid*/
		var tr=obj.parentNode.parentNode;
		var tbody=tr.parentNode;
		var guid=tr.children[0].children[0].value;
		if(window.confirm('<s:text name="problem.table.deleteConfirm"/>')){
			$.ajax({
				type:"post",
				url:"deleteProblem.action",
				data: {"problem.guid" : guid},
 				success:function(str)
				{																					
					zj.showSystemMessage(str);
					if("删除成功"==str)
						tbody.removeChild(tr); 
				},
				error:function(str)
				{
					zj.showSystemMessage(str);
				}
		});
		}
	}
	
	/*导出*/
	function exportProblem(){
		var form = $('#queryForm');
		form.attr("action","exportProblemsByAjax.action");
		form.attr("target","_blank");
		form.submit();
		form.attr("action","problemManagement.action");
		form.attr("target","_self");
	}
	
	/*时间先后顺序校验*/
	function timeValidate(){
		if (document.getElementById("start_time")&&document.getElementById("end_time")
		&&document.getElementById("start_time").value>document.getElementById("end_time").value) {
			zj.showSystemMessage("开始时间应早于结束时间");
			return false;
		}
		return true;
    }
</script>
</head>
<body>
<!-- 问题查询条件以及按钮DIV -->
<div id="queryDiv">
	<s:form action="problemManagement.action" id="queryForm">
	<ul>
	<!-- 学校 -->
	<li>
		<span><s:text name="problem.query.school"/></span>
		<input id="school" type="text" name="problem.school" value="${problem.school_to_show}"/>
	</li>
	<!-- 故障类型 -->	
	<li>
		<span><s:text name="problem.query.faultType"/></span>
		<s:select name="problem.fault_type" list="faultList" listKey="fault_id" listValue="fault_name" headerKey="" headerValue="--  请选择  --"></s:select>
	</li>
	<!-- 处理方案 -->
	<li>
		<span><s:text name="problem.query.solution"/></span>
		<s:select name="problem.solution" list="solutionList" listKey="solution_id" listValue="solution_name" headerKey="" headerValue="--  请选择  --"></s:select>
	</li>
	<li>
		<!-- 查询按钮 -->
		<input type="button" class="btn1" value="<s:text name="problem.button.query"/>" onclick="if(timeValidate())$('#queryForm').submit();">
	</li>
	<!-- 处理结果 -->
	<!--<li>
		<span><s:text name="problem.query.result"/></span>
		<input type="text" name="problem.result" value="${problem.result}">
	</li>
	--><!-- 起始时间 -->
	<li>
		<span><s:text name="problem.query.startTime"/></span>
		<input type="text" id="end_time" name="problem.start_time" readonly="readonly" onfocus="calendar();"
		style="cursor: pointer;text-align: center;" value="${problem.start_time_to_show}"/>
	</li>
	<!-- 结束时间 -->
	<li>
		<span><s:text name="problem.query.endTime"/></span>
		<input type="text" id="end_time" name="problem.end_time" readonly="readonly" onfocus="calendar();"
		style="cursor: pointer;text-align: center;" value="${problem.end_time_to_show}"/>
	</li>
	<!-- 问题状态 -->
	<li>
		<span><s:text name="problem.query.status"/></span>
		<s:select name="problem.problem_status" list="statusList" listKey="status_id" listValue="status_name" headerKey="" headerValue="--  请选择  --"></s:select>
	</li>
	<!-- 重置按钮 -->
	<li>
		<input type="button" class="btn1" value="<s:text name="problem.button.reset"/>" onclick="location.href='./problemManagement.action'">
	</li>
	</ul>
	</s:form>
</div>

<!-- 操作按钮button DIV -->
<div id="buttonDiv" class="buttonImg" style="background-color: #f3f4f4;float:right;padding-left: 500px;">
		<!-- 返回 -->
		<a onclick="location.href='./'" style="float: right;margin-left: 0px;"><s:text name="problem.button.return"/></a>
		<!-- 导出 -->
		<a onclick="exportProblem();" title="<s:text name='problem.query.title.export'/>" style="float: right;margin-left: 0px;"><s:text name="problem.button.export"/></a>
		<!-- 状态更改 -->
		<a onclick="changeStatus();" title="<s:text name='problem.query.title.changeStatus'/>" style="float: right;margin-left: 0px;"><s:text name="problem.button.changeStatus"/></a>
		<!-- 新建问题 -->
		<a onclick="location.href='./getSingleProblem.action?returnNew=true'" title="<s:text name='problem.query.title.insertProblem'/>" style="float: right;margin-left: 0px;"><s:text name='problem.button.insertProblem'/></a>
</div>

<!-- 查询结果table DIV -->
<div id="resultTableDiv">
	<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<th width="3%"><input type="checkbox" id="selAll" onclick="selALl(this);"></th>
			<th width="16%"><s:text name="problem.table.reportTime"/></th>
			<th width="10%"><s:text name="problem.table.school"/></th>
			<th><s:text name="problem.table.reportPerson"/></th>
			<th width="30%"><s:text name="problem.table.faultDescription"/></th>
			<th width="10%"><s:text name="problem.table.solution"/></th>
			<th width="10%"><s:text name="problem.table.status"/></th>
			<th><s:text name="problem.table.operation"/></th>
		</tr>
		<s:iterator value="problemList" status="st">
	        <tr <s:if test="#st.odd == true">class="trBg0" onmouseover="this.className='trOver'"  onmouseout="this.className='trBg0'"</s:if>
	           <s:else>class="trBg1" onmouseover="this.className='trOver'" onmouseout="this.className='trBg1'"</s:else>>
	         	<td><input type="checkbox" name="box" value="<s:property value='guid'/>"></td>
		        <td><s:property value="report_time_to_show"/></td>
		        <td><s:property value="school_to_show"/></td>
		        <td><s:property value="report_person"/></td>
		        <td title="<s:property value="fault_description"/>"><s:property value="fault_description_to_show"/></td>
		        <td><s:property value="solution_to_show"/></td>
		        <td><s:property value="problem_status_to_show"/></td>
		        <td>
		        	<img style="cursor: pointer;width: 20px;margin-right: 5px;" title="<s:text name="problem.table.edit"/>"
		        	 alt="<s:text name="problem.table.edit"/>" src="style/image/edit.png" onclick="window.location='./getSingleProblem.action?problem.guid='+'<s:property value="guid"/>'+'&returnNew=false&page=' + ${page} + '&pageSize=' + ${pageSize}">
		        	<img style="cursor: pointer;width: 20px;margin-right: 5px;" title="<s:text name="problem.table.delete"/>"
		        	 alt="<s:text name="problem.table.delete"/>" src="style/image/delete.png" onclick="removeProblem(this);">
		        </td>
  			</tr>
		</s:iterator>
	
	</table>
	<s:property value="pageBar" escape="false" />
</div>

<!-- 弹出问题状态更改DIV -->
<div id="statusChangeDiv">
	<span><s:text name="problem.table.changeStatus"/></span>
	<ul>
		<s:iterator value="statusList" status="s">
			<li>
				<input type="radio" id="statusToChange" name="statusToChange" value="<s:property value='status_id'/>">
				<SPAN><s:property value="status_name"/></SPAN>
			</li>
		</s:iterator>		
		<li>
			<a class="btn2" onclick="clickOkToChangeStatus();"><s:text name="problem.table.ok"/></a>
		</li>
		<li>
			<a class="btn2" onclick='$("#statusChangeDiv").css("display","none");'><s:text name="problem.table.cancel"/></a>
		</li>
	</ul>

</div>

</body>
</html>