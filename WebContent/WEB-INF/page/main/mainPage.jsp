<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<link rel="stylesheet" href="style/css/ZJ/welcome.css" type="text/css" media="all">
<script type="text/javascript" src="js/graphs/highcharts.js"></script>
<script type="text/javascript" src="js/graphs/exporting.js"></script>
<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: '问题状态预览'
        },
        xAxis: {
            categories: ${statusListJson}
        },
        yAxis: {
        	title: {
                text: ''
            }
        },
        legend: {
            enabled: false
        },
         tooltip: {//鼠标放在图形上时的提示信息  
         formatter: function() {  
                 return '<b>'+ this.series.name +'</b><br/>'+  
                 this.x +': '+ this.y;  
       				}  
         },
        series: [{
            name: '个数',
            data: ${countListJson},
            dataLabels: {
                enabled: true,
                color: '#FFFFFF',
                align: 'right',
                x: -11,
                y: 0,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif',
                    textShadow: '0 0 3px black'
                }
            }
        }]
    });
});
</script>
</head>
<body>
<div id="c_middle">
<!-- <fieldset style="margin-top: 14px;">
<legend id="contentTitle"><s:text name="mainPage.contentTitle"></s:text></legend>
	<ul style="float: left;margin-top: 50px;margin-left: 50px;">
	<s:iterator value="statusList" id="st">
		<li><s:property value="#st.status_name" />:</li>
	</s:iterator>
	</ul>
	<ul style="float: left;margin-top: 50px;">
	<s:iterator value="countList" id="ct">
		<li><s:property value="#ct" /></li>
	</s:iterator>
	</ul>
</fieldset> -->
<div id="container"></div>
<div id="icons" style="margin-top: 20px;">
	<ul>
		<li>
			<img src="style/image/addProblem.png" onclick="location.href='./getSingleProblem.action?returnNew=true'"/>
			<p><s:text name="mainPage.createProblem"></s:text></p>
		</li>
		<li>
			<img src="style/image/problemManagement.png" onclick="window.location='./problemManagement.action'"/>
			<p><s:text name="mainPage.problemManagement"></s:text></p>
		</li>
		<li>
			<img src="style/image/dataConfig.png" onclick="window.location='./faultManagement.action'"/>
			<p><s:text name="mainPage.dataConfiguration" ></s:text></p>
		</li>
		<li>
			<img src="style/image/statistics.png" onclick="window.location='./graph.action'"/>
			<p><s:text name="mainPage.statistics"></s:text></p>
		</li>
	</ul>
</div>


</div>



</body>


</html>


